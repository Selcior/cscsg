namespace cscsg {
    abstract class FormattedText : Text {

        Formatting formatting = new Formatting();

        public override string get() {
            if (Game.settings.textColor >= ColorLevel.COLOR_8) {
                return Color.RESET.getCode() + this.formatting.getANSI() + this.getUnformatted();
            } else {
                return this.getUnformatted();
            }
        }


        public FormattedText format(Formatting formatting) {
            this.formatting = formatting;
            return this;
        }
    }
}