using System.Collections.Generic;
using System.Text;

namespace cscsg
{
    class CompoundText : Text
    {
        List<Text> texts;

        public override string get()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Text x in texts)
            {
                sb.Append(x.get());
            }
            return sb.ToString();
        }

        public override string getUnformatted()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Text x in texts)
            {
                sb.Append(x.getUnformatted());
            }
            return sb.ToString();
        }

        public override CompoundText append(Text text)
        {
            this.texts.Add(text);
            return this;
        }

        public CompoundText()
        {
            this.texts = new List<Text>();
        }
    }
}