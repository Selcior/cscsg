namespace cscsg
{
    abstract class Text
    {
        public virtual string get()
        {
            return getUnformatted();
        }

        public abstract string getUnformatted();

        public virtual CompoundText append(Text text)
        {
            CompoundText result = new CompoundText();
            result.append(this);
            result.append(text);
            return result;
        }

        public virtual CompoundText append(string text)
        {
            return this.append(new LiteralText(text));
        }

        public virtual CompoundText append(string text, Formatting formatting)
        {
            return this.append(new LiteralText(text).format(formatting));
        }
    }
}