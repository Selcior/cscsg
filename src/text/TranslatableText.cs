namespace cscsg
{
    class TranslatableText : FormattedText
    {
        private string key;
        private string[] args;

        public override string getUnformatted()
        {
            string key = this.key;
            if (!Game.settings.textUnicode) {
                if (!key.EndsWith(".nounicode")) {
                    string nounicodekey = $"{key}.nounicode";
                    if (Translation.has(nounicodekey)) {
                        key = nounicodekey;
                    }
                }
            }
            string result = Translation.get(key);
            for (int i = 0; i < args.Length; i++) {
                result = result.Replace("%" + i, args[i]);
            }
            return result;
        }

        public TranslatableText(string key)
        {
            this.key = key;
            this.args = new string[0];
        }

        public TranslatableText(string key, params string[] args)
        {
            this.key = key;
            this.args = args;
        }
    }
}