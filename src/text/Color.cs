using System;

namespace cscsg
{
    struct Color
    {
        bool reset;
        bool extColor;
        byte colorId;


        public bool isReset()
        {
            return this.reset;
        }

        public bool isExtendedColor()
        {
            return this.extColor;
        }

        public byte getColorId()
        {
            return this.colorId;
        }

        public string getCode()
        {
            if (Game.settings.textColor == 0) return "";
            if (reset) return "\u001b[0m";
            if (extColor) return $"\u001b[38;5;{colorId}m";
            if (this.colorId < 8) return $"\u001b[3{colorId}m";
            return $"\u001b[3{colorId-8};1m";
        }

        public string getBackgroundCode()
        {
            if (Game.settings.textColor == 0) return "";
            if (reset) return "\u001b[0m";
            if (extColor) return $"\u001b[48;5;{colorId}m";
            if (this.colorId < 8) return $"\u001b[4{colorId}m";
            return $"\u001b[4{colorId-8};1m";
        }


        public override string ToString() {
            throw new System.Exception("Color.ToString is deprecated");
        }

        public bool isEqualTo(Color c)
        {
            return this.reset == c.reset
            && this.extColor == c.extColor
            && this.colorId == c.colorId;
        }


        public Color(byte colorId, bool extColor = false)
        {
            if (!extColor && colorId >= 16)
            {
                throw new System.InvalidOperationException("A non-extended color's ID cannot be higher than 15");
            }
            this.reset = false;
            this.extColor = extColor;
            this.colorId = colorId;
        }

        public Color()
        {
            this.reset = true;
            this.extColor = false;
            this.colorId = 0;
        }

        public static Color fromRGB(byte red, byte green, byte blue)
        {
            byte r = (byte)((red / 256f) * 6);
            byte g = (byte)((green / 256f) * 6);
            byte b = (byte)((blue / 256f) * 6);
            return new Color((byte)(((r * 6 * 6) + (g * 6) + b) + 16), true);
        }

        public static Color fromGrayscale(float grayscale)
        {
            return new Color((byte)(Math.Min(grayscale * 24f, 23) + 232), true);
        }


        public static readonly Color RESET = new Color();

        public static readonly Color BLACK = new Color(0);
        public static readonly Color RED = new Color(1);
        public static readonly Color GREEN = new Color(2);
        public static readonly Color YELLOW = new Color(3);
        public static readonly Color BLUE = new Color(4);
        public static readonly Color MAGENTA = new Color(5);
        public static readonly Color CYAN = new Color(6);
        public static readonly Color WHITE = new Color(7);

        public static readonly Color BRIGHT_BLACK = new Color(8);
        public static readonly Color BRIGHT_RED = new Color(9);
        public static readonly Color BRIGHT_GREEN = new Color(10);
        public static readonly Color BRIGHT_YELLOW = new Color(11);
        public static readonly Color BRIGHT_BLUE = new Color(12);
        public static readonly Color BRIGHT_MAGENTA = new Color(13);
        public static readonly Color BRIGHT_CYAN = new Color(14);
        public static readonly Color BRIGHT_WHITE = new Color(15);


        public static Color fromName(string colorName) {
            string name = colorName.ToLower();
            switch (name)
            {
                case "black": return Color.BLACK;
                case "red": return Color.RED;
                case "green": return Color.GREEN;
                case "yellow": return Color.YELLOW;
                case "blue": return Color.BLUE;
                case "magenta": return Color.MAGENTA;
                case "cyan": return Color.CYAN;
                case "white": return Color.WHITE;

                default: return Color.RESET; 
            }
        }
    }
}