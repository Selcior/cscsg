using System.Text.Json.Nodes;

namespace cscsg
{
    class ItemStack : JsonSerializable
    {
        public int item;
        public int count;


        // Returns the amount left to add
        public int add(int amount)
        {
            int maxStack = Item.fromId(this.item).getMaxStack();

            if (this.count < maxStack)
            {
                int howManyCanBeAdded = maxStack - this.count;

                if (howManyCanBeAdded >= amount)
                {
                    this.count += amount;
                    return 0;
                }
                else
                {
                    this.count += howManyCanBeAdded;
                    return amount - howManyCanBeAdded;
                }
            }
            else
            {
                return amount;
            }
        }

        // Returns the amount left to remove
        public int remove(int amount)
        {
            if (this.count >= amount)
            {
                this.count -= amount;
                return 0;
            }
            else
            {
                int temp = this.count;
                this.count = 0;
                return amount - temp;
            }
        }


        public ItemStack()
        {
            this.item = 0;
            this.count = 0;
        }

        public ItemStack(int itemId)
        {
            this.item = itemId;
            this.count = 1;
        }

        public ItemStack(int itemId, int count)
        {
            this.item = itemId;
            this.count = count;
        }

        public ItemStack(ItemStack stack)
        {
            this.item = stack.item;
            this.count = stack.count;
        }

        public ItemStack(Item item) : this(item.getId()) { }

        public ItemStack(Item item, int count) : this(item.getId(), count) { }





        public static ItemStack fromJson(JsonObject j)
        {
            ItemStack result = new ItemStack();
            result.loadJson(j);
            return result;
        }

        public virtual bool loadJson(JsonObject j)
        {
            JsonHelper.load(j, "item", ref this.item);
            JsonHelper.load(j, "count", ref this.count);
            return true;
        }

        public virtual JsonObject toJson()
        {
            JsonObject j = new JsonObject();
            j.Add("item", JsonValue.Create(this.item));
            j.Add("count", JsonValue.Create(this.count));
            return j;
        }
    }
}