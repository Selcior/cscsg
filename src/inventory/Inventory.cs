using System.Collections.Generic;
using System.Text.Json.Nodes;

namespace cscsg
{
    class Inventory : JsonSerializable
    {
        public int size;
        public List<ItemStack> items = new List<ItemStack>();

        public ItemStack getSlot(int index)
        {
            if (this.items.Count <= index)
            {
                return null;
            }
            return this.items[index];
        }

        // Remove null ItemStacks, and ItemStacks with count <= 0 or item == 0
        public void cleanup()
        {
            for (int i = 0; i < this.items.Count; i++)
            {
                if (this.items[i] == null || this.items[i].count <= 0 || this.items[i].item == 0)
                {
                    this.items.RemoveAt(i);
                    i--;
                }
            }
        }

        public bool isFull()
        {
            cleanup();
            return this.items.Count >= this.size;
        }

        // Returns amount that did not fit in the inventory
        public int addItem(int itemId, int count)
        {
            int leftToAdd = count;
            int currentSlot = 0;
            while (leftToAdd > 0 && currentSlot < this.size)
            {
                ItemStack currentStack = getSlot(currentSlot);
                if (currentStack == null)
                {
                    currentSlot++;
                    continue;
                }
                if (currentStack.item == itemId)
                {
                    leftToAdd = currentStack.add(leftToAdd);
                }
                currentSlot++;
            }
            cleanup();
            while (leftToAdd > 0 && !isFull())
            {
                ItemStack currentStack = new ItemStack(itemId, 0);
                leftToAdd = currentStack.add(leftToAdd);
                this.items.Add(currentStack);
                currentSlot++;
            }
            return leftToAdd;
        }

        // Returns amount that could not be removed from the inventory
        public int removeItem(int itemId, int count)
        {
            int leftToRemove = count;
            int currentSlot = this.size - 1;
            while (leftToRemove > 0 && currentSlot >= 0)
            {
                ItemStack currentStack = getSlot(currentSlot);
                if (currentStack == null)
                {
                    currentSlot--;
                    continue;
                }
                if (currentStack.item == itemId)
                {
                    leftToRemove = currentStack.remove(leftToRemove);
                }
                currentSlot--;
            }
            cleanup();
            return leftToRemove;
        }

        // Returns slot number of the given item
        public int findItem(int itemId, int from = 0)
        {
            for (int i = from; i < items.Count; i++)
            {
                if (itemId == items[i].item)
                {
                    return i;
                }
            }
            return -1;
        }


        public Inventory() : this(20) { }

        public Inventory(int size)
        {
            this.size = size;
        }





        public static Inventory fromJson(JsonObject j)
        {
            Inventory result = new Inventory();
            result.loadJson(j);
            return result;
        }

        public bool loadJson(JsonObject j)
        {
            List<JsonObject> items = JsonHelper.loadObjArray(j, "items");
            this.items.Clear();
            foreach (JsonObject x in items)
            {
                this.items.Add(ItemStack.fromJson(x));
            }

            return true;
        }

        public JsonObject toJson()
        {
            JsonObject j = new JsonObject();

            JsonArray items = new JsonArray();
            foreach (ItemStack stack in this.items)
            {
                items.Add(stack.toJson());
            }
            j.Add("items", items);

            return j;
        }
    }
}