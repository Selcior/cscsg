using System;

namespace cscsg
{
    class Dialogue
    {
        public Text speakerName = null;
        public Text[] parts;
        public Action<ChoiceData> choiceCallback = null;

        public string getString(int index = 0) {
            return parts[index].getUnformatted();
        }

        public Dialogue(Text part) {
            this.parts = new Text[1];
            this.parts[0] = part;
        }
    }
}