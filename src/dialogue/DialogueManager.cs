using System;
using System.Threading;

namespace cscsg
{
    class DialogueManager
    {

        public static void printSlowRaw(string text, ref bool skip, float speed = 1)
        {
            float baseValue = 1000f / speed;
            for (int i = 0; i < text.Length; i++)
            {
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    skip = true;
                }

                Console.Write(text[i]);
                if (!skip)
                {
                    Thread.Sleep((int)(baseValue / Game.settings.textSpeed));
                }
            }
        }

        public static void printSlowRaw(Text text, ref bool skip, float speed = 1)
        {
            printSlowRaw(text.get(), ref skip, speed);
        }

        public static void printSlow(string text, ref bool skip)
        {
            float baseValue = 1000f;
            for (int i = 0; i < text.Length; i++)
            {
                if (Console.KeyAvailable && Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    skip = true;
                }

                if (text[i] == '%')
                {
                    i++;
                    if (text[i] == 'w')
                    { // 500 ms
                        if (!skip)
                        {
                            Thread.Sleep(500);
                        }
                    }
                    else if (text[i] == 's')
                    { // Slow mode (speed / 2)
                        baseValue *= 2;
                    }
                    else if (text[i] == 'f')
                    { // Fast mode (speed * 2)
                        baseValue /= 2;
                    }
                    else if (text[i] == 'c')
                    { // Color change
                        i++;
                        if (text[i] == '{')
                        {
                            i++;
                            string colorName = "";
                            while (text[i] != '}')
                            {
                                colorName += text[i];
                                i++;
                            }
                            if (Game.settings.textColor >= ColorLevel.COLOR_8)
                            {
                                Console.Write(Formatting.fromName(colorName));
                            }
                        }
                        else
                        {
                            Console.Write(Formatting.RESET_STR);
                        }
                    }
                }
                else
                {
                    Console.Write(text[i]);
                    if (!skip)
                    {
                        Thread.Sleep((int)(baseValue / Game.settings.textSpeed));
                    }
                }
            }
            Console.Write(Formatting.RESET_STR);
        }

        public static void startDialogue(Dialogue dialogue)
        {
            Game.openScreen(new DialogueScreen(dialogue));
        }
    }
}