using System;
using System.Collections.Generic;

namespace cscsg
{

    abstract class Tile
    {
        byte id;
        string nameTKey;
        Item tileItem = null;
        Loot loot = null;
        bool solid = true;
        bool unbreakable = false;
        bool priority = false;


        public virtual byte getId()
        {
            return this.id;
        }

        public virtual Item getTileItem()
        {
            return tileItem;
        }

        public virtual TileChar getWallChar(World world, int x, int y, int z)
        {
            return TC_AIR;
        }

        public virtual TileChar getFloorChar(World world, int x, int y, int z)
        {
            return TC_AIR;
        }


        // Returns the translation key for the name
        public virtual string getName()
        {
            return this.nameTKey;
        }

        // Whether or not this tile is solid on top
        public virtual bool canBeStoodOn()
        {
            return this.isSolid();
        }

        // Whether or not this tile is solid like a wall
        public virtual bool isSolid()
        {
            return this.solid;
        }

        // Whether or not this tile can be climbed on (like a ladder)
        public virtual bool isClimbable()
        {
            return false;
        }

        // Whether or not this tile can be broken
        public virtual bool isUnbreakable()
        {
            return this.unbreakable;
        }

        // Whether or not it renders on top of entities
        public virtual bool hasPriority()
        {
            return this.priority;
        }


        public virtual Loot getLootTable()
        {
            if (this.loot == null)
            {
                this.loot = new Loot().addEntry(new LootEntry(this.id));
            }
            return this.loot;
        }

        public virtual List<ItemStack> getLoot(World world, Random random = null)
        {
            if (random == null)
            {
                random = world.random;
            }
            return this.getLootTable().get(1, random);
        }



        public virtual void entityCollision(World world, Vector3<int> pos, Entity entity)
        {
        }

        // Returns true if the tile was successfully placed
        public virtual bool place(World world, Vector3<int> pos)
        {
            world.setTile(pos, this.getId());
            return true;
        }

        // Returns true if the tile was successfully mined
        public virtual bool mine(World world, Vector3<int> pos, Player player = null)
        {
            world.setTile(pos, Tile.AIR);
            return true;
        }

        // Returns true if the tile item was successfully used
        public virtual bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            return false;
        }



        // Set translation key for the tile name
        protected Tile setName(string name)
        {
            this.nameTKey = name;
            return this;
        }

        protected Tile setSolid(bool solid)
        {
            this.solid = solid;
            return this;
        }

        protected Tile setUnbreakable(bool unbreakable)
        {
            this.unbreakable = unbreakable;
            return this;
        }

        protected Tile setPriority(bool priority)
        {
            this.priority = priority;
            return this;
        }

        protected Tile setLoot(Loot loot)
        {
            this.loot = loot;
            return this;
        }



        public virtual void randomTick(World world, int x, int y, int z)
        {
        }



        protected Tile(byte id)
        {
            this.id = id;
            this.nameTKey = $"tile.id.{id}";
        }

        static Tile[] tiles = new Tile[256];
        public static Tile fromId(byte id)
        {
            return tiles[id];
        }
        public static void addTile(Tile tile)
        {
            tiles[tile.id] = tile;
            tile.tileItem = new TileItem(tile);
        }

        public static TileChar TC_AIR = new TileChar(' ', Formatting.WHITE);

        static Tile()
        {
            addTile(
                new CharTile(AIR, TC_AIR, TC_AIR).setSolid(false).setUnbreakable(true).setName("tile.air")
            );
            addTile(
                new CharTile(GRASS,
                    new TileChar('▓', new Formatting(Color.fromRGB(0x9b, 0x76, 0x53)).setBackground(Color.BLACK)),
                    new TileChar('"', new Formatting(Color.fromRGB(0x10, 0x80, 0x10)).setBackground(Color.GREEN))
                ).setLoot(
                    new Loot().addEntry(new LootEntry(Tile.DIRT))
                ).setName("tile.grass")
            );
            addTile(
                new CharTile(DIRT,
                    new TileChar('▓', new Formatting(Color.fromRGB(0x9b, 0x76, 0x53)).setBackground(Color.BLACK)),
                    new TileChar(';', new Formatting(Color.WHITE).setBackground(Color.fromRGB(0x9b, 0x76, 0x53)))
                ).setName("tile.dirt")
            );
            addTile( // █▓▒░ 
                new CharTile(STONE,
                    new TileChar('▓', new Formatting(Color.fromGrayscale(0.5f)).setBackground(Color.BLACK)),
                    new TileChar('░', new Formatting(Color.WHITE).setBackground(Color.fromGrayscale(0.25f)))
                ).setName("tile.stone")
            );
            addTile(
                new CharTile(BEDROCK,
                    new TileChar('X', new Formatting(Color.WHITE).setBackground(Color.BLACK)),
                    new TileChar('░', new Formatting(Color.WHITE).setBackground(Color.BLACK))
                ).setUnbreakable(true).setName("tile.bedrock")
            );
            addTile(new SaplingTile(SAPLING));
            addTile(
                new TransparentTile(TREE,
                    new TileChar('&', new Formatting(Color.fromRGB(0x9e, 0x6d, 0x3c)).setBackground(Color.GREEN)),
                    new TileChar('░', new Formatting(Color.WHITE).setBackground(Color.GREEN))
                ).setLoot(new Loot(3)
                    .addEntry(new LootEntry(Item.STICK.getId(), 1, 1, 1, 0.5f))
                    .addEntry(new LootEntry(Tile.WOOD, 3, 8, 4, 0.75f))
                    .addEntry(new LootEntry(Tile.SAPLING, 1, 3, 2, 0.75f))
                ).setName("tile.tree")
            );
            addTile(
                new PlantTile(TALL_GRASS,
                    new TileChar('#', new Formatting(Color.fromRGB(0x15, 0x52, 0x17))),
                    TC_AIR
                ).setLoot(new Loot()
                    .addEntry(new LootEntry(Item.STICK.getId(), 1, 1, 1, 0.1f))
                    .addEntry(new LootEntry(Item.SEEDS.getId(), 1, 1, 4, 0.4f))
                ).setSolid(false).setPriority(true).setName("tile.tallGrass")
            );
            addTile(
                new PlantTile(YELLOW_FLOWER,
                    new TileChar('*', new Formatting(Color.YELLOW)),
                    TC_AIR
                ).setSolid(false).setName("tile.yellowFlower")
            );
            addTile(
                new PlantTile(RED_FLOWER,
                    new TileChar('*', new Formatting(Color.RED)),
                    TC_AIR
                ).setSolid(false).setName("tile.redFlower")
            );
            addTile(new WaterTile(WATER).setName("tile.water"));
            addTile(new WaterTile(SHALLOW_WATER, true).setName("tile.shallowWater"));
            addTile(
                new CharTile(SAND,
                    new TileChar('▓', new Formatting(Color.fromRGB(0xde, 0xda, 0x5f)).setBackground(Color.WHITE)),
                    new TileChar('.', new Formatting(Color.WHITE).setBackground(Color.fromRGB(0xde, 0xda, 0x5f)))
                ).setName("tile.sand")
            );
            addTile(
                new CharTile(COAL_ORE,
                    new TileChar(new Char("⁖", ":"), new Formatting(Color.BLACK).setBackground(Color.fromGrayscale(0.50f))),
                    new TileChar(new Char("⁖", ":"), new Formatting(Color.BLACK).setBackground(Color.fromGrayscale(0.25f)))
                ).setName("tile.coalOre")
            );
            addTile(
                new CharTile(GOLD_ORE,
                    new TileChar(new Char("⁘", ":"), new Formatting(Color.YELLOW).setBackground(Color.fromGrayscale(0.50f))),
                    new TileChar(new Char("⁘", ":"), new Formatting(Color.YELLOW).setBackground(Color.fromGrayscale(0.25f)))
                ).setName("tile.goldOre")
            );
            addTile(
                new CharTile(WOOD,
                    new TileChar('[', new Formatting(Color.WHITE).setBackground(Color.fromRGB(0x9e, 0x6d, 0x3c))),
                    new TileChar('\'', new Formatting(Color.BLACK).setBackground(Color.fromRGB(0x78, 0x53, 0x2e)))
                ).setName("tile.wood")
            );
            addTile(new ForestGrassTile(FOREST_GRASS));
            addTile(new RedMushroomTile(RED_MUSHROOM));
            addTile(new BlueMushroomTile(BLUE_MUSHROOM));
            addTile(new YellowMushroomTile(YELLOW_MUSHROOM));
            addTile(new PurpleMushroomTile(PURPLE_MUSHROOM));
            addTile(
                new CharTile(EMERALD_ORE,
                    new TileChar(new Char("⁙", ":"), new Formatting(Color.GREEN).setBackground(Color.fromGrayscale(0.50f))),
                    new TileChar(new Char("⁙", ":"), new Formatting(Color.GREEN).setBackground(Color.fromGrayscale(0.25f)))
                ).setName("tile.emeraldOre")
            );
            addTile(
                new CharTile(RUBY_ORE,
                    new TileChar(new Char("⁙", ":"), new Formatting(Color.RED).setBackground(Color.fromGrayscale(0.50f))),
                    new TileChar(new Char("⁙", ":"), new Formatting(Color.RED).setBackground(Color.fromGrayscale(0.25f)))
                ).setName("tile.rubyOre")
            );
            addTile(
                new CharTile(DIAMOND_ORE,
                    new TileChar(new Char("⁙", ":"), new Formatting(Color.CYAN).setBackground(Color.fromGrayscale(0.50f))),
                    new TileChar(new Char("⁙", ":"), new Formatting(Color.CYAN).setBackground(Color.fromGrayscale(0.25f)))
                ).setName("tile.diamondOre")
            );
            addTile(
                new TransparentTile(COBWEB,
                    new TileChar(new Char("%", ":"), new Formatting(Color.WHITE)),
                    new TileChar(new Char("%", ":"), new Formatting(Color.fromGrayscale(0.50f)))
                ).setName("tile.cobweb")
            );
            // ⁙
            // new CharTile(0xf0, '.');
            // new CharTile(0xf1, ',');
            // new CharTile(0xf2, ':');
            // new CharTile(0xf3, ';');
            // new CharTile(0xf4, '|');
            // new CharTile(0xf5, '{');
            // new CharTile(0xf6, '$');
            // new CharTile(0xf7, '#');
            addTile(
                new CharTile(INVALID,
                    new TileChar('!', new Formatting().setBackground(Color.MAGENTA)),
                    new TileChar('!', new Formatting().setBackground(Color.MAGENTA))
                ).setUnbreakable(true)
            );
        }

        public const byte AIR = 0;
        public const byte GRASS = 1;
        public const byte DIRT = 2;
        public const byte STONE = 3;
        public const byte BEDROCK = 4;
        public const byte SAPLING = 5;
        public const byte TREE = 6;
        public const byte TALL_GRASS = 7;
        public const byte YELLOW_FLOWER = 8;
        public const byte RED_FLOWER = 9;
        public const byte WATER = 10;
        public const byte SHALLOW_WATER = 11;
        public const byte SAND = 12;
        public const byte COAL_ORE = 13;
        public const byte GOLD_ORE = 14;
        public const byte WOOD = 15;
        public const byte FOREST_GRASS = 16;
        public const byte RED_MUSHROOM = 17;
        public const byte BLUE_MUSHROOM = 18;
        public const byte YELLOW_MUSHROOM = 19;
        public const byte PURPLE_MUSHROOM = 20;
        public const byte EMERALD_ORE = 21;
        public const byte RUBY_ORE = 22;
        public const byte DIAMOND_ORE = 23;
        public const byte COBWEB = 24;
        public const byte GRADIENT_0 = 0xf0;
        public const byte GRADIENT_1 = 0xf1;
        public const byte GRADIENT_2 = 0xf2;
        public const byte GRADIENT_3 = 0xf3;
        public const byte GRADIENT_4 = 0xf4;
        public const byte GRADIENT_5 = 0xf5;
        public const byte GRADIENT_6 = 0xf6;
        public const byte GRADIENT_7 = 0xf7;
        public const byte INVALID = 0xff;
    }
}