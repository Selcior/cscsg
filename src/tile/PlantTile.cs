namespace cscsg
{
    class PlantTile : TransparentTile
    {
        public override bool place(World world, Vector3<int> pos)
        {
            byte tileBelow = world.getTileId(pos.x, pos.y - 1, pos.z);
            if (tileBelow == Tile.GRASS || tileBelow == Tile.FOREST_GRASS)
            {
                return base.place(world, pos);
            }
            else
            {
                Game.error(new TranslatableText("command.error.place.wrongFloor.grass"));
                return false;
            }
        }

        public PlantTile(byte id, TileChar wallChar, TileChar floorChar) : base(id, wallChar, floorChar)
        { }
    }
}