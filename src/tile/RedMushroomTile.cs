namespace cscsg
{
    class RedMushroomTile : PlantTile
    {

        public override bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            if (player.health >= player.getMaxHealth())
            {
                return false;
            }
            stack.count--;
            player.addHealth(20);
            return true;
        }

        public RedMushroomTile(byte id) : base(id,
            new TileChar('^', new Formatting(Color.RED)),
            TC_AIR
        )
        {
            this.setSolid(false);
            this.setName("tile.redMushroom");
        }
    }
}