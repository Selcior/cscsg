namespace cscsg
{
    class PurpleMushroomTile : PlantTile
    {

        public override bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            stack.count--;
            player.removeHealth(30);
            player.removeMana(30);
            player.removeEnergy(200);
            return true;
        }

        public PurpleMushroomTile(byte id) : base(id,
            new TileChar('^', new Formatting(Color.fromRGB(0x70, 0x2a, 0xbf))),
            TC_AIR
        )
        {
            this.setSolid(false);
            this.setName("tile.purpleMushroom");
        }
    }
}