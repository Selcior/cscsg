namespace cscsg
{
    class ForestGrassTile : CharTile
    {
        private static byte[] tiles = {
            Tile.RED_FLOWER,
            Tile.YELLOW_FLOWER,
            Tile.RED_MUSHROOM,
            Tile.BLUE_MUSHROOM,
            Tile.YELLOW_MUSHROOM,
            Tile.PURPLE_MUSHROOM,
            Tile.TALL_GRASS
        };

        public override void randomTick(World world, int x, int y, int z)
        {
            if (world.random.NextDouble() < 0.1)
            {
                if (world.getTileId(x, y + 1, z) == AIR)
                {
                    world.setTile(x, y + 1, z, tiles[world.random.Next(tiles.Length)]);
                }
            }
        }

        public ForestGrassTile(byte id) : base(id,
            new TileChar('▓', new Formatting(Color.fromRGB(0x9b, 0x76, 0x53)).setBackground(Color.BLACK)),
            new TileChar('`', new Formatting(Color.GREEN).setBackground(Color.fromRGB(0x1d, 0x7d, 0x20)))
        )
        {
            this.setLoot(new Loot().addEntry(new LootEntry(Tile.DIRT)));
            this.setName("tile.forestGrass");
        }
    }
}