namespace cscsg
{
    class YellowMushroomTile : PlantTile
    {

        public override bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            if (player.energy >= player.getMaxEnergy())
            {
                return false;
            }
            stack.count--;
            player.addEnergy(500);
            return true;
        }

        public YellowMushroomTile(byte id) : base(id,
            new TileChar('^', new Formatting(Color.YELLOW)),
            TC_AIR
        )
        {
            this.setSolid(false);
            this.setName("tile.yellowMushroom");
        }
    }
}