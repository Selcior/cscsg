using System;

namespace cscsg
{
    class LootEntry
    {
        public int itemId;
        public int weight;
        public int minAmount;
        public int maxAmount;
        public float chance;


        // Returns an empty item stack or an item stack with a random count (minAmount <= count < maxAmount)
        public ItemStack get(Random random)
        {
            if (random.NextDouble() < chance)
            {
                return new ItemStack(itemId, random.Next(minAmount, maxAmount));
            }
            return new ItemStack();
        }


        public LootEntry(int itemId, int minAmount, int maxAmount, int weight, float chance)
        {
            this.itemId = itemId;
            this.weight = weight;
            this.minAmount = minAmount;
            this.maxAmount = maxAmount;
            this.chance = chance;
        }

        public LootEntry(int itemId, int minAmount, int maxAmount, int weight)
        : this(itemId, minAmount, maxAmount, weight, 1) { }

        public LootEntry(int itemId, int minAmount, int maxAmount)
        : this(itemId, minAmount, maxAmount, 1) { }

        public LootEntry(int itemId, int count)
        : this(itemId, count, count) { }

        public LootEntry(int itemId)
        : this(itemId, 1) { }

        public LootEntry()
        : this(Item.STICK.getId()) { }
    }
}
