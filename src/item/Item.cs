using System;

namespace cscsg
{
    class Item
    {
        public const int MAX_ITEM_COUNT = 512;

        protected readonly int id;

        // Translation key for name
        protected string nameTKey;
        protected int maxStack;


        public virtual int getId()
        {
            return this.id;
        }

        public virtual string getName()
        {
            return this.nameTKey;
        }

        public virtual int getMaxStack()
        {
            return this.maxStack;
        }

        // Returns true if this should tick the game
        public virtual bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            return false;
        }


        protected Item setMaxStack(int maxStack)
        {
            this.maxStack = maxStack;
            return this;
        }

        protected Item(int id)
        {
            this.id = id;
            this.nameTKey = $"item.id.{id}";
            this.maxStack = 99;
            Item.items[id] = this;
        }

        protected Item(int id, string name)
            : this(id)
        {
            this.nameTKey = name;
        }



        static Item[] items = new Item[MAX_ITEM_COUNT];
        public static Item fromId(int id)
        {
            return items[id];
        }

        public static Item NOTHING = new Item(0, "item.nothing"); // This item will get removed on inventory cleanup
        public static Item STICK = new Item(256, "item.stick");
        public static Item SEEDS = new Item(257, "item.seeds");


    }
}