namespace cscsg
{
    class TileItem : Item
    {
        public Tile getTile()
        {
            return Tile.fromId((byte)this.id);
        }

        public override string getName()
        {
            return this.nameTKey;
        }

        public override int getMaxStack()
        {
            return 99;
        }


        public override bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            return getTile().use(world, pos, player, stack);
        }

        public TileItem(Tile tile) : base(tile.getId())
        {
            this.nameTKey = tile.getName();
        }
    }
}