using System.Collections.Generic;
using System.Text.Json.Nodes;

namespace cscsg
{
    static class JsonHelper
    {
        // If node == null, returns null
        // Returns null if could not be converted
        public static JsonValue asValue(JsonNode node)
        {
            if (node == null)
            {
                return null;
            }
            try
            {
                return node.AsValue();
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        // If node == null, returns null
        // Returns null if could not be converted
        public static JsonArray asArray(JsonNode node)
        {
            if (node == null)
            {
                return null;
            }
            try
            {
                return node.AsArray();
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        // If node == null, returns null
        // Returns null if could not be converted
        public static JsonObject asObject(JsonNode node)
        {
            if (node == null)
            {
                return null;
            }
            try
            {
                return node.AsObject();
            }
            catch (System.Exception)
            {
                return null;
            }
        }





        // If obj == null, returns null
        // Returns null if could not be loaded
        public static JsonNode loadProperty(JsonObject obj, string property)
        {
            if (obj == null)
            {
                return null;
            }

            JsonNode output = null;
            if (obj.TryGetPropertyValue(property, out output))
            {
                return output;
            }
            else
            {
                return null;
            }
        }

        // If obj == null, returns null
        // Returns null if could not be converted or loaded
        public static JsonValue loadValue(JsonObject obj, string property)
        {
            return asValue(loadProperty(obj, property));
        }

        // If obj == null, returns null
        // Returns null if could not be converted or loaded
        public static JsonArray loadArray(JsonObject obj, string property)
        {
            return asArray(loadProperty(obj, property));
        }

        // If obj == null, returns null
        // Returns null if could not be converted or loaded
        public static JsonObject loadObject(JsonObject obj, string property)
        {
            return asObject(loadProperty(obj, property));
        }



        // Returns false on failed load
        public static bool load<T>(JsonObject obj, string property, ref T output)
        {
            JsonValue val = loadValue(obj, property);
            if (val == null)
            {
                return false;
            }

            try
            {
                output = val.GetValue<T>();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        // Returns empty list on error
        public static List<JsonObject> loadObjArray(JsonObject obj, string property)
        {
            List<JsonObject> list = new List<JsonObject>();
            JsonArray jsonList = loadArray(obj, property);
            if (jsonList == null)
            {
                return list;
            }

            foreach (JsonNode it in jsonList)
            {
                JsonObject elem = asObject(it);
                if (elem != null)
                {
                    list.Add(elem);
                }
            }
            return list;
        }

        // Returns empty dict on error
        public static Dictionary<string, JsonObject> loadObjDict(JsonObject obj, string property)
        {
            Dictionary<string, JsonObject> dict = new Dictionary<string, JsonObject>();
            JsonObject jsonDict = loadObject(obj, property);
            if (jsonDict == null)
            {
                return dict;
            }

            foreach (var it in jsonDict)
            {
                JsonObject elem = asObject(it.Value);
                if (elem != null)
                {
                    dict.Add(it.Key, elem);
                }
            }
            return dict;
        }
    }
}