using System.Text.Json.Nodes;

namespace cscsg
{
    interface JsonSerializable
    {
        // Loads json data into an existing object
        // Returns false if data could not be loaded
        bool loadJson(JsonObject j);

        JsonObject toJson();
    }
}