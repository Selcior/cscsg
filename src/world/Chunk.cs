using System;
using System.Collections.Generic;
using System.Text.Json.Nodes;

namespace cscsg
{
    class Chunk : JsonSerializable
    {
        public const int CHUNK_SIZE = 32;
        public const int CHUNK_HEIGHT = CHUNK_SIZE;

        public byte[] tiles;

        public bool isPopulated = false;

        public Vector2<int> position = null;

        // This field is only used with unloaded chunks. If the chunk is loaded, the entities are in World.loadedEntities.
        public List<Entity> entities = new List<Entity>();

        public byte getTileId(int x, int y, int z)
        {
            if (x < 0 || x >= CHUNK_SIZE) return 0xff;
            if (y < 0 || y >= CHUNK_HEIGHT) return 0xff;
            if (z < 0 || z >= CHUNK_SIZE) return 0xff;
            return tiles[(x * CHUNK_HEIGHT * CHUNK_SIZE) + (y * CHUNK_SIZE) + z];
        }

        public Tile getTile(int x, int y, int z)
        {
            return Tile.fromId(getTileId(x, y, z));
        }

        public void setTile(int x, int y, int z, byte tile)
        {
            if (x < 0 || x >= CHUNK_SIZE) return;
            if (y < 0 || y >= CHUNK_HEIGHT) return;
            if (z < 0 || z >= CHUNK_SIZE) return;
            tiles[(x * CHUNK_HEIGHT * CHUNK_SIZE) + (y * CHUNK_SIZE) + z] = tile;
        }

        public void randomTick(World world, int chunkX, int chunkZ)
        {
            int r = world.random.Next();
            int x = (r % CHUNK_SIZE);
            int z = ((r / CHUNK_SIZE) % CHUNK_SIZE);
            int y = (r / CHUNK_SIZE / CHUNK_SIZE) % CHUNK_HEIGHT;

            Tile tile = getTile(x, y, z);
            tile.randomTick(world, (chunkX * CHUNK_SIZE) + x, y, (chunkZ * CHUNK_SIZE) + z);
        }

        public Chunk()
        {
            this.tiles = new byte[CHUNK_SIZE * CHUNK_HEIGHT * CHUNK_SIZE];
        }





        // Returns null if tile data could not be loaded, meaning the chunk needs to be regenerated
        public static Chunk fromJson(JsonObject j)
        {
            Chunk result = new Chunk();
            if (!result.loadJson(j)) return null;
            return result;
        }

        public bool loadJson(JsonObject j)
        {
            string tilesBase64 = null;
            JsonHelper.load(j, "tiles", ref tilesBase64);
            if (tilesBase64 == null)
            {
                return false;
            }
            byte[] newTiles = Convert.FromBase64String(tilesBase64);
            if (newTiles.Length != this.tiles.Length)
            {
                return false;
            }
            this.tiles = newTiles;

            JsonHelper.load(j, "isPopulated", ref this.isPopulated);
            List<JsonObject> entities = JsonHelper.loadObjArray(j, "entities");
            foreach (JsonObject obj in entities)
            {
                this.entities.Add(Entity.fromJson(obj));
            }

            return true;
        }

        public JsonObject toJson()
        {
            JsonObject j = new JsonObject();
            j.Add("tiles", Convert.ToBase64String(this.tiles));
            j.Add("isPopulated", this.isPopulated);

            JsonArray entities = new JsonArray();
            foreach (Entity ent in this.entities)
            {
                if (ent != null) {
                    entities.Add(ent.toJson());
                }
            }
            j.Add("entities", entities);
            return j;
        }
    }

}