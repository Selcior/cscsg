namespace cscsg
{
    class FlatGenerator : WorldGenerator
    {
        public override void generateChunkShape(World world, int chunkX, int chunkZ)
        {
            Chunk c = new Chunk();
            for (int x = 0; x < Chunk.CHUNK_SIZE; x++) {
                for (int z = 0; z < Chunk.CHUNK_SIZE; z++) {
                    c.setTile(x, World.SURFACE_LEVEL, z, Tile.GRASS);
                }
            }

            for (int y = 0; y < World.SURFACE_LEVEL; y++) {
                for (int x = 0; x < Chunk.CHUNK_SIZE; x++) {
                    for (int z = 0; z < Chunk.CHUNK_SIZE; z++) {
                        c.setTile(x, y, z, Tile.STONE);
                    }
                }
            }

            for (int x = 0; x < Chunk.CHUNK_SIZE; x++) {
                for (int z = 0; z < Chunk.CHUNK_SIZE; z++) {
                    c.setTile(x, 0, z, Tile.BEDROCK);
                }
            }
            world.setChunk(chunkX, chunkZ, c);
        }
    }
}