using System;

namespace cscsg
{
    abstract class Decorator
    {
        public abstract void decorate(World world, int chunkX, int chunkZ, Random random);
    }
}