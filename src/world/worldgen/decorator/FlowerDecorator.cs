namespace cscsg
{
    class FlowerDecorator : PatchDecorator
    {
        public FlowerDecorator(byte tileId) : base(2, 7, 0.1f, 0f, tileId, Tile.GRASS) { }
    }
}