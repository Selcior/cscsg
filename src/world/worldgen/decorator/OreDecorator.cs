using System;

namespace cscsg
{
    abstract class OreDecorator : Decorator
    {
        protected float innerRadius;
        protected float outerRadius;
        protected float maxChance;
        protected float minChance;
        protected byte tileId;
        protected byte replacedTileId;

        protected virtual byte getTileId()
        {
            return tileId;
        }

        public override void decorate(World world, int chunkX, int chunkZ, Random random)
        {
            int offsetInChunkX = random.Next(0, Chunk.CHUNK_SIZE);
            int offsetInChunkZ = random.Next(0, Chunk.CHUNK_SIZE);

            int offsetX = (chunkX * Chunk.CHUNK_SIZE) + offsetInChunkX;
            int offsetZ = (chunkZ * Chunk.CHUNK_SIZE) + offsetInChunkZ;
            int offsetY;
            {
                double r = random.NextDouble();
                offsetY = (int)(r * r * r * World.SURFACE_LEVEL);
            }

            for (int x = (int)-outerRadius; x <= outerRadius; x++)
            {
                for (int y = (int)-outerRadius; y <= outerRadius; y++)
                {
                    for (int z = (int)-outerRadius; z <= outerRadius; z++)
                    {
                        float distance = (float)Math.Sqrt((x * x) + (z * z));
                        float distanceInRing = 1 - ((distance - innerRadius) / (outerRadius - innerRadius));
                        float chance = Math.Max(Math.Min(
                            (distanceInRing * (maxChance - minChance)) + minChance,
                        maxChance), minChance);

                        if (random.NextDouble() < chance) {
                            if (world.getTileId(offsetX + x, offsetY + y, offsetZ + z, true) == this.replacedTileId) {
                                world.setTile(offsetX + x, offsetY + y, offsetZ + z, getTileId(), true);
                            }
                        }
                    }
                }
            }
        }

        protected OreDecorator(float innerRadius, float outerRadius, float maxChance, float minChance, byte tileId, byte replacedTileId)
        {
            this.innerRadius = innerRadius;
            this.outerRadius = outerRadius;
            this.maxChance = maxChance;
            this.minChance = minChance;
            this.tileId = tileId;
            this.replacedTileId = replacedTileId;
        }
    }
}