using System;

namespace cscsg
{
    class BasicOreDecorator : OreDecorator
    {
        public int amount;

        public override void decorate(World world, int chunkX, int chunkZ, Random random)
        {
            for (int i = 0; i < amount; i++)
            {
                base.decorate(world, chunkX, chunkZ, random);
            }
        }

        public BasicOreDecorator(byte tileId, int size, int amount = 1, byte replacedTileId = Tile.STONE)
            : base(size - 2, size + 1, 1f, 0f, tileId, replacedTileId)
        {
            this.amount = amount;
        }
    }
}