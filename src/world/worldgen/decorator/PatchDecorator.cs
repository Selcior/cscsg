using System;

namespace cscsg
{
    class PatchDecorator : Decorator
    {
        protected float innerRadius;
        protected float outerRadius;
        protected float maxChance;
        protected float minChance;
        protected byte tileId;
        protected byte bottomTileId;
        protected float decoratorChance;

        protected virtual byte getTileId()
        {
            return tileId;
        }

        public override void decorate(World world, int chunkX, int chunkZ, Random random)
        {
            if (random.NextDouble() > this.decoratorChance) {
                return;
            }
            
            int offsetInChunkX = random.Next(0, Chunk.CHUNK_SIZE);
            int offsetInChunkZ = random.Next(0, Chunk.CHUNK_SIZE);

            int offsetX = (chunkX * Chunk.CHUNK_SIZE) + offsetInChunkX;
            int offsetZ = (chunkZ * Chunk.CHUNK_SIZE) + offsetInChunkZ;

            for (int x = (int)-outerRadius; x <= outerRadius; x++)
            {
                for (int z = (int)-outerRadius; z <= outerRadius; z++)
                {
                    float distance = (float)Math.Sqrt((x * x) + (z * z));
                    float distanceInRing = 1 - ((distance - innerRadius) / (outerRadius - innerRadius));
                    float chance = Math.Max(Math.Min(
                        (distanceInRing * (maxChance - minChance)) + minChance,
                    maxChance), minChance);

                    if (random.NextDouble() < chance) {
                        int y = world.getSurfaceLevel(offsetX + x, offsetZ + z);
                        if (world.getTileId(offsetX + x, y, offsetZ + z) == this.bottomTileId) {
                            world.setTile(offsetX + x, y + 1, offsetZ + z, getTileId(), true);
                        }
                    }
                }
            }
        }

        protected PatchDecorator(float innerRadius, float outerRadius, float maxChance, float minChance, byte tileId, byte bottomTileId, float decoratorChance = 1f)
        {
            this.innerRadius = innerRadius;
            this.outerRadius = outerRadius;
            this.maxChance = maxChance;
            this.minChance = minChance;
            this.tileId = tileId;
            this.bottomTileId = bottomTileId;
            this.decoratorChance = decoratorChance;
        }
    }
}