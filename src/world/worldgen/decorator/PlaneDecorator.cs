using System;

namespace cscsg
{
    class PlaneDecorator : Decorator
    {
        protected float chance;
        protected byte tileId;
        protected byte bottomTileId;
        protected float decoratorChance;

        protected virtual byte getTileId()
        {
            return tileId;
        }

        public override void decorate(World world, int chunkX, int chunkZ, Random random)
        {
            if (random.NextDouble() > this.decoratorChance) {
                return;
            }

            int offsetX = chunkX * Chunk.CHUNK_SIZE;
            int offsetZ = chunkZ * Chunk.CHUNK_SIZE;

            for (int x = 0; x < Chunk.CHUNK_SIZE; x++)
            {
                for (int z = 0; z < Chunk.CHUNK_SIZE; z++)
                {
                    if (random.NextDouble() < this.chance) {
                        int y = world.getSurfaceLevel(offsetX + x, offsetZ + z);
                        if (world.getTileId(offsetX + x, y, offsetZ + z) == this.bottomTileId) {
                            world.setTile(offsetX + x, y + 1, offsetZ + z, getTileId(), true);
                        }
                    }
                }
            }
        }

        protected PlaneDecorator(float chance, byte tileId, byte bottomTileId, float decoratorChance = 1f)
        {
            this.chance = chance;
            this.tileId = tileId;
            this.bottomTileId = bottomTileId;
            this.decoratorChance = decoratorChance;
        }
    }
}