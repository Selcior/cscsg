namespace cscsg
{
    class GrassDecorator : PatchDecorator
    {
        public GrassDecorator(byte bottomTile) : base(5, 9, 0.25f, 0f, Tile.TALL_GRASS, bottomTile) { }
    }
}