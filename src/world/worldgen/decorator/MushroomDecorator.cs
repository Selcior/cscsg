namespace cscsg
{
    class MushroomDecorator : PlaneDecorator
    {
        public MushroomDecorator(byte tileId, float rarity = 0.05f) : base(rarity, tileId, Tile.FOREST_GRASS) { }
    }
}