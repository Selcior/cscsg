using System.Collections.Generic;
using System;
using System.IO;
using System.Text.Json.Nodes;

namespace cscsg
{
    class World
    {
        public const int SURFACE_LEVEL = 20;
        public const int RANDOM_TICK_RATE = 30;

        public string filename = "";
        public string name = "";
        public Random random;
        public int entityCounter = 0;

        private int age = 0;
        private int seed = 0;
        public bool cheated = false;
        public Player player;

        public int animalSpawnTimer = 10;
        public int monsterSpawnTimer = 200;

        private Dictionary<long, Chunk> loadedChunks = new Dictionary<long, Chunk>();
        private HashSet<long> chunksToUnload = new HashSet<long>();
        private List<Entity> loadedEntities = new List<Entity>();

        private Dictionary<long, Region> cachedRegions = new Dictionary<long, Region>();

        private WorldGenerator generator;




        public int getSeed()
        {
            return this.seed;
        }


        // Returns a combination of x and z 
        public static long toCoordsId(int x, int z)
        {
            return (long)(((ulong)x << 32) + (uint)z);
        }

        // Returns a combination of both vector components
        public static long toCoordsId(Vector2<int> coords)
        {
            return toCoordsId(coords.x, coords.y);
        }

        // Returns a combination of x and z 
        public static Vector2<int> fromCoordsId(long id)
        {
            return new Vector2<int>((int)(id >> 32), (int)id);
        }


        // Returns a combination of x and z 
        public static int toShortCoordsId(short x, short z)
        {
            return (int)(((uint)x << 32) + (ushort)z);
        }


        public static int getChunkSeed(int worldSeed, int chunkX, int chunkZ)
        {
            Random random = new Random(worldSeed);
            random.NextDouble();
            return (int)(((uint)toShortCoordsId((short)chunkX, (short)chunkZ)) ^ (uint)random.Next());
        }

        public int getChunkSeed(int chunkX, int chunkZ)
        {
            return getChunkSeed(this.seed, chunkX, chunkZ);
        }

        public static int getEntitySeed(int worldSeed, int worldAge, int entityCounter)
        {
            Random random = new Random(worldSeed);
            random.NextDouble();
            return (int)((uint)(((uint)worldAge << 16) + (ushort)entityCounter) ^ (uint)random.Next());
        }

        public int getEntitySeed()
        {
            return getEntitySeed(this.seed, this.age, this.entityCounter);
        }



        // Returns x, z in chunk of the block at position pos
        public Vector2<int> toChunkCoords(Vector2<int> pos)
        {
            return toChunkCoords(pos.x, pos.y);
        }

        // Returns x, z in chunk of the block at position x, z
        public static Vector2<int> toChunkCoords(int x, int z)
        {
            Vector2<int> chunkPos = getChunkPos(x, z);
            return new Vector2<int>(
                x - (chunkPos.x * Chunk.CHUNK_SIZE),
                z - (chunkPos.y * Chunk.CHUNK_SIZE)
            );
        }


        // Returns x, z in region of the chunk at position pos
        public static Vector2<int> toRegionCoords(Vector2<int> pos)
        {
            return toRegionCoords(pos.x, pos.y);
        }

        // Returns x, z in region of the chunk at position x, z
        public static Vector2<int> toRegionCoords(int x, int z)
        {
            Vector2<int> regionPos = getRegionPos(x, z);
            return new Vector2<int>(
                x - (regionPos.x * Region.REGION_SIZE),
                z - (regionPos.y * Region.REGION_SIZE)
            );
        }



        // Returns chunkX, chunkZ of the block at position pos
        public static Vector2<int> getChunkPos(Vector2<int> pos)
        {
            return getChunkPos(pos.x, pos.y);
        }

        // Returns chunkX, chunkZ of the block at position x, z
        public static Vector2<int> getChunkPos(int x, int z)
        {
            return new Vector2<int>(
                (int)Math.Floor((double)x / Chunk.CHUNK_SIZE),
                (int)Math.Floor((double)z / Chunk.CHUNK_SIZE)
            );
        }


        // Returns regionX, regionZ of the chunk at position pos
        public static Vector2<int> getRegionPos(Vector2<int> pos)
        {
            return getRegionPos(pos.x, pos.y);
        }

        // Returns regionX, regionZ of the chunk at position x, z
        public static Vector2<int> getRegionPos(int x, int z)
        {
            return new Vector2<int>(
                (int)Math.Floor((double)x / Region.REGION_SIZE),
                (int)Math.Floor((double)z / Region.REGION_SIZE)
            );
        }



        public int getAge()
        {
            return age;
        }

        public void addEntity(Entity entity)
        {
            if (this.getEntity(entity.getUuid()) == null)
            {
                this.loadedEntities.Add(entity);
                entity.spawn(this);
                entityCounter++;
            }
            else
            {
                // Entity was already added before, dont add it again
            }
        }

        public void removeEntity(Guid uuid)
        {
            this.loadedEntities.RemoveAll(delegate (Entity e)
            {
                return e.getUuid() == uuid;
            });
        }





        public Region getRegion(int regionX, int regionZ, bool createIfNull = false)
        {
            long id = World.toCoordsId(regionX, regionZ);
            if (!this.cachedRegions.ContainsKey(id))
            {
                this.cachedRegions.Add(id, Region.load(regionX, regionZ, this.filename));
            }
            if (createIfNull)
            {
                if (this.cachedRegions[id] == null)
                {
                    this.cachedRegions[id] = new Region();
                }
            }
            return this.cachedRegions[id];
        }


        // Loads one chunk from the appropriate region file
        public Chunk loadChunk(int chunkX, int chunkZ)
        {
            Vector2<int> regionPos = getRegionPos(chunkX, chunkZ);
            string path = Path.Join("world", this.filename, "region", $"r_{regionPos.x}_{regionPos.y}.json.gz");
            Vector2<int> coordsInRegion = toRegionCoords(chunkX, chunkZ);
            long chunkInRegionId = toCoordsId(coordsInRegion.x, coordsInRegion.y);

            if (!File.Exists(path))
            {
                return null;
            }
            Region reg = getRegion(regionPos.x, regionPos.y);
            if (reg == null)
            {
                return null;
            }
            if (!reg.chunks.ContainsKey(chunkInRegionId))
            {
                return null;
            }
            return reg.chunks[chunkInRegionId];
        }


        // Returns true if the chunk needs to be generated, false if loaded from file or if it was already loaded in memory
        // Warning: it also loads the chunk from file into loaded chunks
        public bool tryLoadChunk(int chunkX, int chunkZ)
        {
            if (this.loadedChunks.ContainsKey(toCoordsId(chunkX, chunkZ)))
            {
                return false;
            }
            Chunk c = loadChunk(chunkX, chunkZ);
            if (c != null)
            {
                setChunk(chunkX, chunkZ, c);
                return false;
            }
            return true;
        }

        // Returns true if the chunk was generated, false if loaded from file or if it was already loaded in memory
        public bool loadOrGenerateChunkShape(int chunkX, int chunkZ)
        {
            bool result = tryLoadChunk(chunkX, chunkZ);
            if (result)
            {
                generateChunkShape(chunkX, chunkZ);
            }
            return result;
        }

        // Returns true if the chunk was generated or decorated, false if loaded from file or if it was already loaded in memory
        public bool loadOrGenerateChunk(int chunkX, int chunkZ)
        {
            bool result = tryLoadChunk(chunkX, chunkZ);
            if (result)
            {
                generateChunk(chunkX, chunkZ);
            }
            else
            {
                Chunk c = getChunk(chunkX, chunkZ);
                if (!c.isPopulated)
                {
                    populateChunk(chunkX, chunkZ);
                    result = true;
                }
            }
            return result;
        }

        public void unloadChunk(int chunkX, int chunkZ)
        {
            unloadChunk(toCoordsId(chunkX, chunkZ));
        }

        public void unloadChunk(long chunkId)
        {
            this.chunksToUnload.Add(chunkId);
        }



        public static World load(string worldName)
        {
            string worldDir = Path.Join("world", worldName);
            if (Directory.Exists(worldDir))
            {
                World world = new World();
                world.filename = worldName;

                JsonObject j = JsonHelper.asObject(JsonLoader.loadFile(Path.Join(worldDir, "worldinfo.json")));
                JsonHelper.load(j, "age", ref world.age);
                if (!JsonHelper.load(j, "seed", ref world.seed))
                {
                    world.seed = Game.random.Next();
                }
                JsonHelper.load(j, "cheated", ref world.cheated);
                world.player = Player.fromJson(JsonHelper.loadObject(j, "player"));

                return world;
            }
            else
            {
                return null;
            }
        }

        public void saveInfo()
        {
            string worldDir = Path.Join("world", this.filename);

            JsonObject j = new JsonObject();
            j.Add("age", JsonValue.Create(this.age));
            j.Add("seed", JsonValue.Create(this.seed));
            j.Add("cheated", JsonValue.Create(this.cheated));
            j.Add("player", this.player.toJson());

            JsonLoader.saveFile(Path.Join(worldDir, "worldinfo.json"), j);
        }



        public void save()
        {
            foreach (long chunkId in this.chunksToUnload)
            {
                Chunk c = this.loadedChunks[chunkId];
                c.entities.Clear();

                // Add entities to chunk
                for (int i = 0; i < this.loadedEntities.Count; i++)
                {
                    Entity ent = this.loadedEntities[i];
                    if (ent is Player)
                    {
                        continue;
                    }

                    if (toCoordsId(getChunkPos(ent.pos.x, ent.pos.z)) == chunkId)
                    {
                        c.entities.Add(ent);
                        this.loadedEntities.RemoveAt(i);
                        i--;
                    }
                }


                // Load region if needed
                Vector2<int> regionPos = getRegionPos(fromCoordsId(chunkId));
                long regionId = toCoordsId(regionPos);
                Region reg = getRegion(regionPos.x, regionPos.y, true);

                // Add chunk to region
                long posInRegion = toCoordsId(toRegionCoords(fromCoordsId(chunkId)));
                reg.chunks[posInRegion] = c;

            }

            foreach (var pair in cachedRegions)
            {
                Vector2<int> regionPos = fromCoordsId(pair.Key);
                pair.Value.save(regionPos.x, regionPos.y, this.filename);
            }

            foreach (long chunkId in this.chunksToUnload)
            {
                this.loadedChunks[chunkId].entities = new List<Entity>();

                // Remove chunk from loaded chunks
                this.loadedChunks.Remove(chunkId);
            }
            this.chunksToUnload.Clear();
            this.cachedRegions.Clear();

            this.saveInfo();
        }

        public void saveAll()
        {
            foreach (var pair in loadedChunks)
            {
                unloadChunk(pair.Key);
            }
            try
            {
                save();
            }
            catch (System.Exception e)
            {
                Game.error(e.ToString());
                return;
            }
            int chunksGenerated = autoLoad();
            if (chunksGenerated > 0)
            {
                Game.printLn(
                    new TranslatableText("info.generatedChunks.whileLoading", chunksGenerated.ToString())
                    .format(Formatting.YELLOW)
                );
            }
            Game.printLn(new TranslatableText("info.save", this.filename).format(Formatting.GREEN));
        }

        // Retrurns the count of generated chunks
        public int autoLoad()
        {
            Vector2<int> chunkPos = getChunkPos(Game.player.pos.x, Game.player.pos.z);
            int chunksGenerated = 0;
            for (int x = -3; x <= 3; x++)
            {
                for (int z = -3; z <= 3; z++)
                {
                    Chunk c = getChunk(x + chunkPos.x, z + chunkPos.y);
                    if (c == null || !c.isPopulated)
                    {
                        if (loadOrGenerateChunk(x + chunkPos.x, z + chunkPos.y))
                        {
                            chunksGenerated++;
                        }
                    }

                    long chunkId = toCoordsId(x + chunkPos.x, z + chunkPos.y);
                    if (this.chunksToUnload.Contains(chunkId))
                    {
                        this.chunksToUnload.Remove(chunkId);
                        c = getChunk(x + chunkPos.x, z + chunkPos.y);
                    }
                }
            }
            return chunksGenerated;
        }


        public void autoUnload()
        {
            foreach (var pair in this.loadedChunks)
            {
                long chunkId = pair.Key;
                Vector2<int> pos = World.fromCoordsId(chunkId);
                Vector2<int> playerPos = World.getChunkPos(Game.player.pos.x, Game.player.pos.z);
                if (pos.x < playerPos.x - 2 || pos.x > playerPos.x + 2 ||
                    pos.y < playerPos.y - 2 || pos.y > playerPos.y + 2)
                {
                    unloadChunk(chunkId);
                }
            }
        }



        public HashSet<long> getChunksToUnload()
        {
            return this.chunksToUnload;
        }

        public Chunk getChunk(int chunkX, int chunkZ)
        {
            long chunkId = toCoordsId(chunkX, chunkZ);
            if (loadedChunks.ContainsKey(chunkId))
            {
                return loadedChunks[chunkId];
            }
            else
            {
                return null;
            }
        }

        public Chunk getChunkOfTile(int x, int z)
        {
            Vector2<int> coords = getChunkPos(x, z);
            return this.getChunk(coords.x, coords.y);
        }

        public byte getTileId(int x, int y, int z, bool generateNew = false)
        {
            Chunk c = getChunkOfTile(x, z);
            if (c == null)
            {
                if (!generateNew)
                {
                    return Tile.INVALID;
                }

                Vector2<int> chunkPos = getChunkPos(x, z);
                this.loadOrGenerateChunkShape(chunkPos.x, chunkPos.y);
                c = getChunk(chunkPos.x, chunkPos.y);
            }

            Vector2<int> coords = toChunkCoords(x, z);
            return c.getTileId(coords.x, y, coords.y);
        }

        public Tile getTile(int x, int y, int z)
        {
            Chunk c = getChunkOfTile(x, z);
            if (c == null) return Tile.fromId(Tile.INVALID);

            Vector2<int> coords = toChunkCoords(x, z);
            return c.getTile(coords.x, y, coords.y);
        }

        public Tile getTile(Vector3<int> pos)
        {
            return getTile(pos.x, pos.y, pos.z);
        }

        public bool isInbounds(int x, int z)
        {
            Chunk c = getChunkOfTile(x, z);
            return c != null;
        }

        public void setTile(int x, int y, int z, byte id, bool generateNew = false)
        {
            Chunk c = getChunkOfTile(x, z);
            if (c == null)
            {
                if (!generateNew)
                {
                    return;
                }


                Vector2<int> chunkPos = getChunkPos(x, z);
                this.loadOrGenerateChunkShape(chunkPos.x, chunkPos.y);
                c = getChunk(chunkPos.x, chunkPos.y);
            }

            Vector2<int> coords = toChunkCoords(x, z);
            c.setTile(coords.x, y, coords.y, id);
        }

        public void setTile(Vector3<int> pos, byte id, bool generateNew = false)
        {
            setTile(pos.x, pos.y, pos.z, id, generateNew);
        }


        public List<Entity> getEntities()
        {
            return this.loadedEntities;
        }

        public Entity getEntity(Guid uuid)
        {
            return this.loadedEntities.Find(delegate (Entity e)
            {
                return e.getUuid() == uuid;
            });
        }

        public List<Entity> getEntitiesAt(int x, int y, int z)
        {
            List<Entity> entities = loadedEntities.FindAll(delegate (Entity e)
            {
                return e.pos.x == x && e.pos.y == y && e.pos.z == z;
            });
            return entities;
        }


        public List<Entity> getEntitiesAt(Vector3<int> pos)
        {
            return getEntitiesAt(pos.x, pos.y, pos.z);
        }



        public void setChunk(int chunkX, int chunkZ, Chunk c)
        {
            this.loadedChunks[toCoordsId(chunkX, chunkZ)] = c;
            foreach (Entity entity in c.entities)
            {
                this.addEntity(entity);
            }
            c.position = new Vector2<int>(chunkX, chunkZ);
        }



        public int getSurfaceLevel(int x, int z)
        {
            int y = Chunk.CHUNK_HEIGHT - 1;
            byte tile = getTileId(x, y, z);
            while (tile == Tile.AIR && y >= 0)
            {
                y--;
                tile = getTileId(x, y, z);
            }
            return y;
        }



        public void generateChunk(int chunkX, int chunkZ)
        {
            this.generator.fullGenerateChunk(this, chunkX, chunkZ);
        }

        public void generateChunkShape(int chunkX, int chunkZ)
        {
            this.generator.generateChunkShape(this, chunkX, chunkZ);
        }

        public void populateChunk(int chunkX, int chunkZ)
        {
            this.generator.populateChunk(this, chunkX, chunkZ);
        }

        public void generate(int width, int height)
        {
            for (int x = -(width / 2); x <= (width / 2); x++)
            {
                for (int z = -(height / 2); z <= (height / 2); z++)
                {
                    this.generateChunk(x, z);
                }
            }
        }

        public void spawnItemStack(ItemStack stack, Vector3<int> position)
        {
            ItemEntity ent = new ItemEntity(stack);
            ent.pos = position;
            this.addEntity(ent);
        }

        public int getMobcap(MobcapType type)
        {
            int count = 0;
            foreach (Entity e in this.loadedEntities)
            {
                if (e.getMobcapType() == type)
                {
                    count++;
                }
            }
            return count;
        }


        public void tick()
        {
            if (--animalSpawnTimer <= 0)
            {
                animalSpawnTimer = this.random.Next(100, 150);

                if (getMobcap(MobcapType.ANIMAL) < 25)
                {
                    int count = this.random.Next(3, 8);
                    for (int i = 0; i < count; i++)
                    {
                        Pig pig = new Pig();
                        pig.pos = new Vector3<int>(this.random.Next(-50, 50) + player.pos.x, World.SURFACE_LEVEL + 1, this.random.Next(-50, 50) + player.pos.z);
                        this.addEntity(pig);
                    }
                }
            }
            if (--monsterSpawnTimer <= 0)
            {
                monsterSpawnTimer = this.random.Next(150, 200);

                if (getMobcap(MobcapType.MONSTER) < 15)
                {
                    int count = this.random.Next(3, 8);
                    for (int i = 0; i < count; i++)
                    {
                        Zombie zombie = new Zombie();
                        zombie.pos = new Vector3<int>(this.random.Next(-50, 50) + player.pos.x, World.SURFACE_LEVEL + 1, this.random.Next(-50, 50) + player.pos.z);
                        this.addEntity(zombie);
                    }
                }
                // [TODO] spawn monsters
            }

            foreach (Entity e in this.getEntities())
            {
                e.tick(this);
            }

            for (int i = 0; i < this.getEntities().Count; i++)
            {
                Entity e = this.getEntities()[i];
                if (e.isDespawned())
                {
                    removeEntity(e.getUuid());
                    i--;
                }
            }

            // Randomtick chunks
            foreach (var pair in this.loadedChunks)
            {
                Chunk c = pair.Value;
                for (int i = 0; i < World.RANDOM_TICK_RATE; i++)
                {
                    Vector2<int> coords = fromCoordsId(pair.Key);
                    c.randomTick(this, coords.x, coords.y);
                }
            }

            // [TODO] spawn mobs
            // [TODO] mobcaps


            // Check if you need to generate new chunks
            int chunksGenerated = autoLoad();
            if (chunksGenerated > 0)
            {
                Game.printPersistent(
                    new TranslatableText("info.generatedChunks", chunksGenerated.ToString())
                        .format(Formatting.GREEN),
                    10
                );
            }

            age++;
        }

        public World() : this(0)
        { }

        public World(int seed) : this(seed, new NoiseGenerator()
                .addDecorator(new TreeDecorator())
                .addDecorator(new ForestDecorator())
                .addDecorator(new MushroomDecorator(Tile.RED_MUSHROOM))
                .addDecorator(new MushroomDecorator(Tile.BLUE_MUSHROOM))
                .addDecorator(new MushroomDecorator(Tile.YELLOW_MUSHROOM, 0.03f))
                .addDecorator(new MushroomDecorator(Tile.PURPLE_MUSHROOM, 0.03f))
                .addDecorator(new MushroomDecorator(Tile.TALL_GRASS))
                .addDecorator(new FlowerDecorator(Tile.YELLOW_FLOWER))
                .addDecorator(new FlowerDecorator(Tile.RED_FLOWER))
                .addDecorator(new GrassDecorator(Tile.GRASS))
                .addDecorator(new BasicOreDecorator(Tile.DIAMOND_ORE, 1, 1))
                .addDecorator(new BasicOreDecorator(Tile.RUBY_ORE, 1, 3))
                .addDecorator(new BasicOreDecorator(Tile.EMERALD_ORE, 1, 4))
                .addDecorator(new BasicOreDecorator(Tile.GOLD_ORE, 2, 2))
                .addDecorator(new BasicOreDecorator(Tile.COAL_ORE, 4, 5))
                .addDecorator(new BasicOreDecorator(Tile.DIAMOND_ORE, 1, 2, Tile.COAL_ORE))
        )
        { }

        public World(int seed, WorldGenerator generator)
        {
            this.seed = seed;
            this.generator = generator;
            this.random = new Random(seed);
        }
    }
}