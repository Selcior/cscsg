using System.Text.Json.Nodes;

namespace cscsg
{
    class Vector2<T> : JsonSerializable
    {
        public T x;
        public T y;

        public static Vector2<T> operator +(Vector2<T> a, Vector2<T> b) {
            dynamic ax = a.x, bx = b.x, ay = a.y, by = b.y;
            return new Vector2<T>(ax + bx, ay + by);
        }
        
        public Vector2(T x, T y)
        {
            this.x = x;
            this.y = y;
        }

        public Vector2()
        {
            this.x = default(T);
            this.y = default(T);
        }
        
        public Vector2(Vector2<T> vec)
        {
            this.x = vec.x;
            this.y = vec.y;
        }





        public static Vector2<T> fromJson(JsonObject j)
        {
            Vector2<T> result = new Vector2<T>();
            if (!result.loadJson(j)) return null;
            return result;
        }

        public bool loadJson(JsonObject j)
        {
            Vector2<T> vec = new Vector2<T>();
            if (!JsonHelper.load(j, "x", ref vec.x)) return false;
            if (!JsonHelper.load(j, "y", ref vec.y)) return false;
            this.x = vec.x;
            this.y = vec.y;
            return true;
        }

        public virtual JsonObject toJson()
        {
            JsonObject obj = new JsonObject();
            obj.Add("x", JsonValue.Create(this.x));
            obj.Add("y", JsonValue.Create(this.y));
            return obj;
        }
    }
}