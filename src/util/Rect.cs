namespace cscsg
{
    class Rect<T>
    {
        public T x;
        public T y;
        public T w;
        public T h;
        
        public Vector2<T> pos {
            get {
                return new Vector2<T>(x, y);
            }
            set {
                this.x = value.x;
                this.y = value.y;
            }
        }
        
        public Vector2<T> size {
            get {
                return new Vector2<T>(w, h);
            }
            set {
                this.w = value.x;
                this.h = value.y;
            }
        }

        public Rect(T x, T y, T w, T h)
        {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
        }

        public Rect(T w, T h)
        {
            this.x = default(T);
            this.y = default(T);
            this.w = w;
            this.h = h;
        }

        public Rect(Vector2<T> pos, Vector2<T> size)
        {
            this.x = pos.x;
            this.y = pos.y;
            this.w = size.x;
            this.h = size.y;
        }

        public Rect(Vector2<T> size)
        {
            this.x = default(T);
            this.y = default(T);
            this.w = size.x;
            this.h = size.y;
        }
    }
}