using System.Text.Json.Nodes;

namespace cscsg
{
    class Vector3<T> : JsonSerializable
    {
        public T x;
        public T y;
        public T z;

        public static Vector3<T> operator +(Vector3<T> a, Vector3<T> b) {
            dynamic ax = a.x, bx = b.x, ay = a.y, by = b.y, az = a.z, bz = b.z;
            return new Vector3<T>(ax + bx, ay + by, az + bz);
        }
        
        public Vector3(T x, T y, T z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vector3()
        {
            this.x = default(T);
            this.y = default(T);
            this.z = default(T);
        }
        
        public Vector3(Vector3<T> vec)
        {
            this.x = vec.x;
            this.y = vec.y;
            this.z = vec.z;
        }

        public static Vector3<int> fromDirection(Direction dir, int magnitude = 1) {
            Vector3<int> result = new Vector3<int>();
            if (dir == Direction.NORTH) result.z = -magnitude;
            else if (dir == Direction.SOUTH) result.z = magnitude;
            else if (dir == Direction.EAST) result.x = magnitude;
            else if (dir == Direction.WEST) result.x = -magnitude;
            else if (dir == Direction.UP) result.y = magnitude;
            else if (dir == Direction.DOWN) result.y = -magnitude;
            return result;
        }





        public static Vector3<T> fromJson(JsonObject j)
        {
            Vector3<T> result = new Vector3<T>();
            if (!result.loadJson(j)) return null;
            return result;
        }

        public bool loadJson(JsonObject j)
        {
            Vector3<T> vec = new Vector3<T>();
            if (!JsonHelper.load(j, "x", ref vec.x)) return false;
            if (!JsonHelper.load(j, "y", ref vec.y)) return false;
            if (!JsonHelper.load(j, "z", ref vec.z)) return false;
            this.x = vec.x;
            this.y = vec.y;
            this.z = vec.z;
            return true;
        }

        public virtual JsonObject toJson()
        {
            JsonObject obj = new JsonObject();
            obj.Add("x", JsonValue.Create(this.x));
            obj.Add("y", JsonValue.Create(this.y));
            obj.Add("z", JsonValue.Create(this.z));
            return obj;
        }
    }
}