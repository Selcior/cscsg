using System.Collections.Generic;


namespace cscsg
{
    class Levenshtein
    {
        public static int getEditDistance(string str1, string str2) {
            int[,] array = new int[str1.Length + 1, str2.Length + 1];
            for (int i = 0; i < array.GetLength(0); i++) {
                array[i, 0] = i;
            }
            for (int i = 0; i < array.GetLength(1); i++) {
                array[0, i] = i;
            }

            for (int x = 1; x < array.GetLength(0); x++) {
                for (int y = 1; y < array.GetLength(1); y++) {
                    if (str1[x - 1] != str2[y - 1]) {
                        int min = array[x - 1, y];
                        if (array[x, y - 1] < min) min = array[x, y - 1];
                        if (array[x - 1, y - 1] < min) min = array[x - 1, y - 1];
                        array[x, y] = min + 1;
                    } else {
                        array[x, y] = array[x - 1, y - 1];
                    }
                }
            }

            return array[array.GetLength(0) - 1, array.GetLength(1) - 1];
        }

        public static string getNearest(List<string> list, string str) {
            string result = null;
            int lowestDistance = int.MaxValue;

            foreach (string s in list) {
                int editDistance = getEditDistance(s, str);
                // Game.printLn($"#####check s={s} str={str} editDistance={editDistance}");
                if (editDistance <= lowestDistance) {
                    lowestDistance = editDistance;
                    result = s;
                }
            }

            // Game.printLn($"#####result={result}");

            return result;
        }
    }
}