namespace cscsg {
    public static class Util {
        public static string center(string str, int length, char fillChar = ' ') {
            string result = "";
            if (result.Length < length) {
                int newLength = (length - str.Length) / 2;
                if (newLength < 0) newLength = 0;

                result += new string(fillChar, newLength);
                result += str;
                result += new string(fillChar, newLength);
            }
            if (result.Length < length) {
                result += fillChar;
            }
            return result;
        }
    }
}