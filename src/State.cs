namespace cscsg
{
    class State
    {
        public Mode mode = Mode.TEXT;
        public int restTime = 0;
        public bool forceRest = false;
        public bool extraInfo = false;
        public bool debugMode = false;
        public bool keyModeAvaliable = true;
    }
}