using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Security.Cryptography;

namespace cscsg
{
    class Game
    {
        public static Player player
        {
            get
            {
                return Game.world.player;
            }
            set
            {
                Game.world.player = value;
            }
        }
        public static World world;
        public static WorldRenderer renderer;
        public static Settings settings;
        public static State state;
        public static Screen screen;

        // Randomly seeded pseudo random generator
        public static Random random = new Random();

        private static Vector3<int> selection = new Vector3<int>();
        private static int selectedEntity = 0;

        private static bool running = false;


        public static Vector3<int> getAbsoluteSelection()
        {
            return player.pos + Game.selection;
        }

        public static Vector3<int> getSelection()
        {
            return Game.selection;
        }

        public static void setSelection(Vector3<int> sel)
        {
            selection = sel;
            if (selection.x > player.reach) selection.x = player.reach;
            if (selection.x < -player.reach) selection.x = -player.reach;
            if (selection.z > player.reach) selection.z = player.reach;
            if (selection.z < -player.reach) selection.z = -player.reach;
            if (selection.y < -1) selection.y = -1;
            if (selection.y > 1) selection.y = 1;
        }

        public static int getSelectedEntityIndex()
        {
            return Game.selectedEntity;
        }

        public static void setSelectedEntityIndex(int index)
        {
            Game.selectedEntity = index;
            List<Entity> entities = getSelectedEntities();
            if (Game.selectedEntity >= entities.Count) Game.selectedEntity = entities.Count - 1;
            if (Game.selectedEntity < 0) Game.selectedEntity = 0;
        }

        public static Entity getSelectedEntity()
        {
            List<Entity> entities = getSelectedEntities();
            if (Game.selectedEntity >= entities.Count) Game.selectedEntity = entities.Count - 1;
            if (Game.selectedEntity < 0) Game.selectedEntity = 0;
            if (entities.Count == 0) return null;
            return entities[Game.selectedEntity];
        }

        public static List<Entity> getSelectedEntities()
        {
            return world.getEntitiesAt(Game.getAbsoluteSelection());
        }



        private static bool shouldTick = false;
        public static void setShouldTick()
        {
            shouldTick = true;
        }


        public static void printLn(Text text)
        {
            printLn(text.get());
        }

        public static void printLn(string text)
        {
            renderer.printLn(text);
        }

        public static void printPersistent(Text text, int ticks)
        {
            printPersistent(text.get(), ticks);
        }

        public static void printPersistent(string text, int ticks)
        {
            printPersistent(new PersistentText(text, ticks));
        }

        public static void printPersistent(PersistentText text)
        {
            renderer.printPersistent(text);
        }

        public static void clearText()
        {
            renderer.clearText();
        }

        public static void error(Text text)
        {
            error(text.get());
        }

        public static void error(string text)
        {
            printLn(new TranslatableText("error.errorMessage").format(Formatting.RED.setBold(true)).append(text));
        }


        public static void openScreen(Screen screen)
        {
            screen.parent = Game.screen;
            Game.screen = screen;
            screen.open();
        }

        public static void closeScreen()
        {
            Screen screenToDelete = Game.screen;
            screenToDelete.close();
            Game.screen = screenToDelete.parent;
            if (Game.screen == null)
            {
                Game.stop();
            }
        }

        public static void replaceScreen(Screen screen)
        {
            screen.parent = Game.screen;
            Game.screen = screen;
        }


        public static void loadSettings()
        {
            Settings s = Settings.load(Settings.FILENAME, false);
            if (s == null)
            {
                s = new Settings();
                s.save(Settings.FILENAME);
            }
            Game.settings = s;
        }

        public static void stop()
        {
            running = false;
            Console.ResetColor();
        }

        public static void init(string[] args)
        {
            state = new State();
            renderer = new WorldRenderer();
            
            loadSettings();

            Translation.reload(settings.lang);

            const string title = "C-Sharp Console Sandbox Game v1.0.0";
            Console.Title = title;
            Console.WriteLine(title);



            // Get player name
            string playerName = null;
            Console.WriteLine(new TranslatableText("intro.name").get());
            {
                string input = Console.ReadLine().Trim();
                if (input.Length > 0)
                {
                    playerName = input;
                }
            }



            if (!Directory.Exists("world"))
            {
                Directory.CreateDirectory("world");
            }

            // Show list of worlds
            string[] worlds = Directory.GetDirectories("world");
            if (worlds.Length > 0)
            {
                Console.WriteLine(new TranslatableText("intro.worlds").get());
            }
            foreach (string worldName in worlds)
            {
                Console.WriteLine(new TranslatableText("intro.world", Path.GetFileName(worldName)).get());
            }


            // Get world name
            string worldFilename = "world";
            Console.WriteLine(new TranslatableText("intro.worldName").get());
            {
                string input = Console.ReadLine().Trim();
                if (input.Length > 0)
                {
                    worldFilename = input;
                }
            }


            world = World.load(worldFilename);

            // If creating a new world, get seed
            if (world == null)
            {
                Console.WriteLine(new TranslatableText("intro.seed").get());
                int worldSeed;
                string input = Console.ReadLine().Trim();
                if (input.Length > 0)
                {
                    try
                    {
                        worldSeed = Convert.ToInt32(input);
                    }
                    catch (System.Exception)
                    {
                        using (HashAlgorithm algorithm = SHA256.Create())
                        {

                            byte[] bytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(input));
                            worldSeed = BitConverter.ToInt32(bytes);
                        }
                    }
                }
                else
                {
                    worldSeed = Game.random.Next();
                }

                world = new World(worldSeed);
                world.filename = worldFilename;
            }


            bool newPlayer = player == null;
            if (newPlayer)
            {
                player = new Player();
            }
            if (playerName != null)
            {
                player.name = playerName;
            }
            world.addEntity(player);
            world.autoLoad();
            world.tick();

            Command.init();

            screen = new WorldScreen();
            if (newPlayer)
            {
                DialogueManager.startDialogue(new Dialogue(new TranslatableText("dialogue.intro")));
            }

            Console.CancelKeyPress += delegate
            {
                Game.stop();
            };




            if (settings == null) return;

            Game.running = true;
            do
            {
                Game.loop();
            }
            while (Game.running);
        }

        public static void loop()
        {
            if (Game.shouldTick)
            {
                world.tick();

                if (world.getAge() % 20 == 0)
                {
                    world.autoUnload();
                    world.autoLoad();
                    if (Game.state.debugMode)
                    {
                        Game.printPersistent(new TranslatableText("debug.markedChunks").format(Formatting.BLUE), 5);
                    }
                }

                if (world.getAge() % 300 == 0)
                {
                    try
                    {
                        world.save();
                        Game.printPersistent(new TranslatableText("info.autosave", world.filename).format(Formatting.GREEN), 10);
                    }
                    catch (System.Exception e)
                    {
                        Game.error(e.ToString());
                        return;
                    }
                }

                if (player.health <= 0)
                {
                    player.die(world);
                }

                shouldTick = false;
            }
            screen.render();
            if (Game.state.restTime <= 0)
            {
                screen.input();
            }
            else
            {
                Game.player.addEnergy(10);
                Game.state.restTime--;
                if (Game.player.energy >= Game.player.getMaxEnergy() && !Game.state.forceRest)
                {
                    Game.state.restTime = 0;
                }
                Game.shouldTick = Game.state.restTime > 0;
                Thread.Sleep(50);
            }
            settings.update();
        }

        public static void Main(string[] args)
        {
            Game.init(args);
        }
    }
}