using System;
using System.Collections.Generic;

namespace cscsg
{
    class InventoryScreen : Screen
    {
        public const int LINES = 3;

        public int selectedItem = 0;

        public override void render()
        {
            // [TODO] this and some other screens should probably be changed to use stringbuilder instead of console
            Settings s = Game.settings;
            Console.Clear();
            Console.Write($"{Formatting.REVERSED}");
            Console.Write(Util.center(new TranslatableText("screen.inventory.title").getUnformatted(), s.getScreenWidth()));
            Console.WriteLine($"{Formatting.RESET_STR}");

            int scroll = selectedItem;

            Inventory inv = Game.player.inventory;

            if (scroll + Game.settings.getScreenHeight() > inv.items.Count) scroll = inv.items.Count - Game.settings.getScreenHeight();
            if (scroll < 0) scroll = 0;

            for (int i = scroll; i < scroll + Game.settings.getScreenHeight() - LINES; i++)
            {
                if (inv.items.Count == 0 && i == 0)
                {
                    Console.WriteLine("Empty");
                }
                else if (i < inv.items.Count)
                {
                    ItemStack stack = inv.getSlot(i);
                    if (stack != null)
                    {
                        if (i == selectedItem)
                        {
                            Console.Write(Formatting.REVERSED);
                            Console.Write(" > ");
                        }

                        Item item = Item.fromId(stack.item);
                        Console.Write(new TranslatableText("screen.inventory.item",
                            (i + 1).ToString(),
                            new TranslatableText(item.getName()).getUnformatted(),
                            stack.count.ToString(),
                            item.getMaxStack().ToString()
                        ).getUnformatted());

                        if (i == selectedItem)
                        {
                            Console.Write(" < ");
                            Console.Write(Formatting.RESET_STR);
                        }
                        Console.WriteLine();

                    }
                    else
                    {
                        Console.WriteLine("null");
                    }
                }
                else
                {
                    Console.WriteLine();
                }
            }

            string hint = Game.state.keyModeAvaliable ? "screen.inventory.hint.key" : "screen.inventory.hint.text";
            Console.Write($"{Formatting.REVERSED}");
            Console.Write(Util.center(new TranslatableText(hint).getUnformatted(), s.getScreenWidth()));
            Console.WriteLine($"{Formatting.RESET_STR}");
        }

        public override void input()
        {
            World world = Game.world;
            Inventory inv = Game.player.inventory;

            try
            {
                ConsoleKeyInfo key = Console.ReadKey();
                Game.state.keyModeAvaliable = true;
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        selectedItem--;
                        break;

                    case ConsoleKey.PageUp:
                        selectedItem -= Game.settings.getScreenHeight() - LINES;
                        break;

                    case ConsoleKey.Home:
                        selectedItem = 0;
                        break;

                    case ConsoleKey.DownArrow:
                        selectedItem++;
                        break;

                    case ConsoleKey.PageDown:
                        selectedItem += Game.settings.getScreenHeight() - LINES;
                        break;

                    case ConsoleKey.End:
                        selectedItem = int.MaxValue;
                        break;

                    case ConsoleKey.T:
                        Game.player.throwItemStack(world, selectedItem);
                        break;

                    case ConsoleKey.Enter:
                    case ConsoleKey.Spacebar:
                        Game.player.selectedItem = selectedItem;
                        Game.closeScreen();
                        break;

                    case ConsoleKey.Escape:
                        Game.closeScreen();
                        break;
                }
            }
            catch (System.InvalidOperationException)
            {
                Game.state.keyModeAvaliable = false;
                string line = Console.ReadLine();
                if (line == "u") selectedItem--;
                if (line == "d") selectedItem++;
                if (line == "t") Game.player.throwItemStack(world, selectedItem);
                if (line == "s") Game.player.selectedItem = selectedItem;
                if (line == "q" || line == "quit") Game.closeScreen();
            }

            if (selectedItem >= inv.items.Count) selectedItem = inv.items.Count - 1;
            if (selectedItem < 0) selectedItem = 0;
        }
    }
}