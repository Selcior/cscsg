using System;
using System.Collections.Generic;

namespace cscsg
{
    class HelpScreen : PagerScreen
    {
        protected static List<Text> generalHelpPage = new List<Text>();
        protected static List<Text> textHelpPage = new List<Text>();
        protected static List<Text> keyHelpPage = new List<Text>();
        protected static List<List<Text>> pages = new List<List<Text>>();
        protected int pageId = 0;

        public override List<Text> getLines()
        {
            return pages[this.pageId];
        }

        public override string getHintTKey()
        {
            return Game.state.keyModeAvaliable ? "screen.help.hint.key" : "screen.help.hint.text";
        }

        protected void changePage(int delta)
        {
            int oldPage = this.pageId;
            this.pageId += delta;
            if (this.pageId >= pages.Count)
            {
                this.pageId = pages.Count - 1;
            }
            if (this.pageId < 0)
            {
                this.pageId = 0;
            }
            if (oldPage != this.pageId) {
                this.scroll = 0;
                updateScroll();
            }
        }

        public override void handleKey(ConsoleKeyInfo key)
        {
            base.handleKey(key);
            if (key.Key == ConsoleKey.LeftArrow)
            {
                changePage(-1);
            }
            if (key.Key == ConsoleKey.RightArrow)
            {
                changePage(1);
            }
        }

        public override void handleText(string text)
        {
            base.handleText(text);
            if (text == "p")
            {
                changePage(-1);
            }
            if (text == "r")
            {
                changePage(1);
            }
        }

        public HelpScreen(Mode mode) : base("screen.help.title")
        {
            this.pageId = 0;
            if (mode == Mode.TEXT)
            {
                this.pageId = 1;
            }
            else if (mode == Mode.KEY)
            {
                this.pageId = 2;
            }
        }

        static HelpScreen()
        {
            generalHelpPage.Add(new TranslatableText("screen.help.general.title"));
            generalHelpPage.Add(new LiteralText(""));
            generalHelpPage.Add(new TranslatableText("help.general.modes.title"));
            generalHelpPage.Add(new TranslatableText("help.general.modes"));
            generalHelpPage.Add(new LiteralText(""));
            generalHelpPage.Add(new TranslatableText("help.general.visibility.title"));
            generalHelpPage.Add(new TranslatableText("help.general.visibility"));
            generalHelpPage.Add(new LiteralText(""));
            generalHelpPage.Add(new TranslatableText("help.general.cursor.title"));
            generalHelpPage.Add(new TranslatableText("help.general.cursor"));
            generalHelpPage.Add(new LiteralText(""));
            generalHelpPage.Add(new TranslatableText("help.general.prompt.title"));
            generalHelpPage.Add(new TranslatableText("help.general.prompt"));
            pages.Add(generalHelpPage);

            textHelpPage.Add(new TranslatableText("screen.help.text.title"));
            textHelpPage.Add(new LiteralText(""));
            textHelpPage.Add(new TranslatableText("help.text.w"));
            textHelpPage.Add(new TranslatableText("help.text.s"));
            textHelpPage.Add(new TranslatableText("help.text.a"));
            textHelpPage.Add(new TranslatableText("help.text.d"));
            textHelpPage.Add(new TranslatableText("help.text.W"));
            textHelpPage.Add(new TranslatableText("help.text.S"));
            textHelpPage.Add(new TranslatableText("help.text.A"));
            textHelpPage.Add(new TranslatableText("help.text.D"));
            textHelpPage.Add(new TranslatableText("help.text.i"));
            textHelpPage.Add(new TranslatableText("help.text.k"));
            textHelpPage.Add(new TranslatableText("help.text.j"));
            textHelpPage.Add(new TranslatableText("help.text.l"));
            textHelpPage.Add(new TranslatableText("help.text.I"));
            textHelpPage.Add(new TranslatableText("help.text.K"));
            textHelpPage.Add(new TranslatableText("help.text.J"));
            textHelpPage.Add(new TranslatableText("help.text.L"));
            textHelpPage.Add(new TranslatableText("help.text.="));
            textHelpPage.Add(new TranslatableText("help.text.-"));
            textHelpPage.Add(new TranslatableText("help.text.["));
            textHelpPage.Add(new TranslatableText("help.text.]"));
            textHelpPage.Add(new TranslatableText("help.text.r"));
            textHelpPage.Add(new TranslatableText("help.text.R"));
            textHelpPage.Add(new TranslatableText("help.text.q"));
            textHelpPage.Add(new TranslatableText("help.text.mine"));
            textHelpPage.Add(new TranslatableText("help.text.f"));
            textHelpPage.Add(new TranslatableText("help.text.place"));
            textHelpPage.Add(new TranslatableText("help.text.use"));
            textHelpPage.Add(new TranslatableText("help.text.e"));
            textHelpPage.Add(new TranslatableText("help.text.interact"));
            textHelpPage.Add(new TranslatableText("help.text.inv"));
            textHelpPage.Add(new TranslatableText("help.text.help"));
            textHelpPage.Add(new TranslatableText("help.text.p"));
            textHelpPage.Add(new TranslatableText("help.text.colors"));
            textHelpPage.Add(new TranslatableText("help.text.tiles"));
            textHelpPage.Add(new TranslatableText("help.text.items"));
            textHelpPage.Add(new TranslatableText("help.text.info"));
            textHelpPage.Add(new TranslatableText("help.text.pos"));
            textHelpPage.Add(new TranslatableText("help.text.rt"));
            textHelpPage.Add(new TranslatableText("help.text.save"));
            textHelpPage.Add(new TranslatableText("help.text.saveas"));
            textHelpPage.Add(new TranslatableText("help.text.seed"));
            textHelpPage.Add(new TranslatableText("help.text.settings"));
            textHelpPage.Add(new TranslatableText("help.text.exit"));
            textHelpPage.Add(new LiteralText(""));
            textHelpPage.Add(new TranslatableText("screen.help.text.debug"));
            textHelpPage.Add(new TranslatableText("help.text.debug"));
            textHelpPage.Add(new TranslatableText("help.text.chkdbg"));
            textHelpPage.Add(new TranslatableText("help.text.testd"));
            textHelpPage.Add(new TranslatableText("help.text.give"));
            textHelpPage.Add(new TranslatableText("help.text.givelots"));
            textHelpPage.Add(new TranslatableText("help.text.take"));
            textHelpPage.Add(new TranslatableText("help.text.spawn"));
            pages.Add(textHelpPage);

            keyHelpPage.Add(new TranslatableText("screen.help.key.title"));
            keyHelpPage.Add(new LiteralText(""));
            keyHelpPage.Add(new TranslatableText("help.key.w"));
            keyHelpPage.Add(new TranslatableText("help.key.s"));
            keyHelpPage.Add(new TranslatableText("help.key.a"));
            keyHelpPage.Add(new TranslatableText("help.key.d"));
            keyHelpPage.Add(new TranslatableText("help.key.w.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.s.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.a.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.d.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.up"));
            keyHelpPage.Add(new TranslatableText("help.key.down"));
            keyHelpPage.Add(new TranslatableText("help.key.left"));
            keyHelpPage.Add(new TranslatableText("help.key.right"));
            keyHelpPage.Add(new TranslatableText("help.key.up.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.down.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.left.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.right.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.subtract"));
            keyHelpPage.Add(new TranslatableText("help.key.equals"));
            keyHelpPage.Add(new TranslatableText("help.key.9"));
            keyHelpPage.Add(new TranslatableText("help.key.0"));
            keyHelpPage.Add(new TranslatableText("help.key.o"));
            keyHelpPage.Add(new TranslatableText("help.key.p"));
            keyHelpPage.Add(new TranslatableText("help.key.r"));
            keyHelpPage.Add(new TranslatableText("help.key.r.shift"));
            keyHelpPage.Add(new TranslatableText("help.key.q"));
            keyHelpPage.Add(new TranslatableText("help.key.f"));
            keyHelpPage.Add(new TranslatableText("help.key.e"));
            keyHelpPage.Add(new TranslatableText("help.key.i"));
            keyHelpPage.Add(new TranslatableText("help.key.h"));
            keyHelpPage.Add(new TranslatableText("help.key.escape"));
            keyHelpPage.Add(new LiteralText(""));
            keyHelpPage.Add(new TranslatableText("screen.help.key.debug"));
            keyHelpPage.Add(new TranslatableText("help.key.f2"));
            keyHelpPage.Add(new TranslatableText("help.key.u"));
            pages.Add(keyHelpPage);
        }
    }
}