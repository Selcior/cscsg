using System.Collections.Generic;

namespace cscsg
{
    class ColorScreen : PagerScreen
    {

        public List<Text> lines = new List<Text>();
        public override List<Text> getLines() {
            return this.lines;
        }

        public ColorScreen() : base("screen.colors.title"){
            for (int i = 0; i < 256; i++) {
                Formatting f = new Formatting().setBackground(new Color((byte)i, true));
                lines.Add(new TranslatableText("screen.colors.color", i.ToString("000")).append($"  {f}  {Formatting.RESET_STR}"));
            }
            for (float i = 0; i < 1; i += 0.02f) {
                Formatting f = new Formatting().setBackground(Color.fromGrayscale(i));
                lines.Add(new TranslatableText("screen.colors.grayscale", i.ToString("0.00")).append($"  {f}  {Formatting.RESET_STR}"));
            }
        }
    }
}