using System;

namespace cscsg
{
    class GameOverScreen : Screen
    {
        public override void render()
        {
            Settings s = Game.settings;
            int screenWidth = s.getScreenWidth();
            int screenHeight = s.getScreenHeight() - 1;
            int half = screenHeight / 2;

            for (int i = 0; i < screenHeight; i++)
            {
                if (i == half)
                {
                    Console.WriteLine(Util.center(new TranslatableText("screen.gameOver.header").getUnformatted(), screenWidth));
                }
                else if (i == half + 1)
                {
                    Console.WriteLine(Util.center(new TranslatableText("screen.gameOver.subtitle").getUnformatted(), screenWidth));
                }
                else
                {
                    Console.WriteLine();
                }
            }
        }

        public override void input()
        {
            Console.ReadLine();
            Game.closeScreen();
        }
    }
}