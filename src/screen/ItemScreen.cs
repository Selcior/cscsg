using System.Collections.Generic;
using System.Text;

namespace cscsg
{
    class ItemScreen : PagerScreen
    {

        public List<Text> lines = new List<Text>();
        public override List<Text> getLines() {
            return this.lines;
        }

        public ItemScreen() : base("screen.items.title"){
            bool doubleChar = Game.settings.textDoubleWidth;

            for (int i = 0; i < Item.MAX_ITEM_COUNT; i++) {
                Item item = Item.fromId(i);
                if (item != null) {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(new TranslatableText("screen.items.item", i.ToString("000")).getUnformatted())
                        .Append('\t')
                        .Append(Translation.get(item.getName()))
                        .Append(" /")
                        .Append(item.getMaxStack());

                    lines.Add(new LiteralText(sb.ToString()));
                }
            }
        }
    }
}