namespace cscsg
{
    class BoolSetting : NamedSetting
    {
        protected bool value = false;
        protected string trueTKey;
        protected string falseTKey;

        public override object get()
        {
            return value;
        }

        public override void set()
        {
            value = !value;
        }

        public override Text getValueText()
        {
            if (this.value)
            {
                return new TranslatableText(trueTKey);
            }
            else
            {
                return new TranslatableText(falseTKey);
            }
        }

        public BoolSetting(string nameTKey, bool value, string trueTKey = "settings.true", string falseTKey = "settings.false") : base(nameTKey)
        {
            this.value = value;
            this.trueTKey = trueTKey;
            this.falseTKey = falseTKey;
        }
    }
}