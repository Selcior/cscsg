namespace cscsg
{
    abstract class NamedSetting : Setting
    {
        public string nameTKey = "setting.undefined";

        public abstract Text getValueText();
        public override Text getText()
        {
            return new TranslatableText(nameTKey, getValueText().getUnformatted());
        }

        protected NamedSetting(string nameTKey)
        {
            this.nameTKey = nameTKey;
        }
    }
}