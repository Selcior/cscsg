using System;

namespace cscsg
{
    class IntSetting : NumberSetting<int>
    {
        public override object get()
        {
            return value;
        }

        public override void set()
        {
            Console.WriteLine("Input number:");
            string line = Console.ReadLine();
            if (line == "")
            {
                if (allowNull)
                {
                    value = null;
                }
            }
            try
            {
                value = Convert.ToInt32(line);
                if (value > max)
                {
                    value = max;
                }
                if (value < min)
                {
                    value = min;
                }
            }
            catch (Exception)
            { }
        }

        public IntSetting(string nameTKey, int value, int min = int.MinValue, int max = int.MaxValue, bool allowNull = false, string nullTKey = "settings.null")
            : base(nameTKey, value, min, max, allowNull, nullTKey)
        { }

        public IntSetting(string nameTKey, Nullable<int> value, int min = int.MinValue, int max = int.MaxValue, bool allowNull = false, string nullTKey = "settings.null")
            : base(nameTKey, value, min, max, allowNull, nullTKey)
        { }
    }
}