using System;

namespace cscsg
{
    abstract class NumberSetting<T> : NamedSetting where T : struct
    {
        protected bool allowNull;
        protected string nullTKey;
        protected Nullable<T> value = null;
        protected T min;
        protected T max;

        public override object get()
        {
            return value;
        }

        public override Text getValueText()
        {
            if (this.value.HasValue)
            {
                return new LiteralText(this.value.Value.ToString());
            }
            else
            {
                return new TranslatableText(this.nullTKey);
            }
        }

        public NumberSetting(string nameTKey, T value, T min, T max, bool allowNull = false, string nullTKey = "settings.null") : base(nameTKey)
        {
            this.value = value;
            this.min = min;
            this.max = max;
            this.allowNull = allowNull;
            this.nullTKey = nullTKey;
        }

        public NumberSetting(string nameTKey, Nullable<T> value, T min, T max, bool allowNull = false, string nullTKey = "settings.null") : base(nameTKey)
        {
            this.value = value;
            this.min = min;
            this.max = max;
            this.allowNull = allowNull;
            this.nullTKey = nullTKey;
        }
    }
}