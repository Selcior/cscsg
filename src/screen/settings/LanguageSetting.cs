using System;
using System.IO;

namespace cscsg
{
    class LanguageSetting : NamedSetting
    {
        protected string value = null;

        public override object get()
        {
            return value;
        }

        public override void set()
        {
            string path = Path.Join("res", "lang");
            if (Directory.Exists(path))
            {
                string[] langs = Directory.GetFiles(path);

                if (langs.Length > 0)
                {
                    Console.WriteLine(new TranslatableText("setting.lang.found").get());
                }
                foreach (string lang in langs)
                {
                    Console.WriteLine(new TranslatableText("setting.lang.file", Path.GetFileNameWithoutExtension(lang)).get());
                }


                Console.WriteLine(new TranslatableText("setting.lang.input").get());
                {
                    string input = Console.ReadLine().Trim();
                    if (File.Exists(Path.Join(path, input + ".json")))
                    {
                        this.value = input;
                    }
                    else
                    {
                        this.value = "en_us";
                    }
                }
            }
        }

        public override Text getValueText()
        {
            return new LiteralText(this.value.ToString());
        }

        public LanguageSetting(string nameTKey, string value) : base(nameTKey)
        {
            this.value = value;
        }
    }
}