namespace cscsg
{
    enum ColorLevel
    {
        NO_COLOR,
        COLOR_8,
        COLOR_16,
        COLOR_256
    }
}