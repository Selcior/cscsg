namespace cscsg
{
    class Camera {
        public Vector3<int> position = new Vector3<int>();

        public Rect<int> getRect(int screenWidth, int screenHeight, int realScreenHeight) {
            int usedHeight;
            if (realScreenHeight - 5 <= screenHeight) {
                usedHeight = realScreenHeight;
            } else {
                usedHeight = screenHeight;
            }
            return new Rect<int>(
                position.x - (screenWidth / 2),
                position.z - (usedHeight / 2),
                screenWidth,
                screenHeight
            );
        }
    }
}