using System;

namespace cscsg
{

    class KeyCommand : Command
    {

        public readonly ConsoleKey key;
        public Func<ConsoleKeyInfo, bool> callback;

        public override bool matchKey(ConsoleKeyInfo key)
        {
            if (key.Key == this.key)
            {
                return this.callback.Invoke(key);
            }
            return false;
        }

        public KeyCommand(ConsoleKey key)
        {
            this.key = key;
        }

        public KeyCommand(ConsoleKey key, Func<ConsoleKeyInfo, bool> callback)
        {
            this.key = key;
            this.callback = callback;
        }



        public static new void init()
        {
            World world = Game.world;
            allCommands.Add(new KeyCommand(ConsoleKey.H, (key) =>
            {
                Game.openScreen(new HelpScreen(Mode.KEY));
                return true;
            }));

            allCommands.Add(new KeyCommand(ConsoleKey.W, (key) =>
            {
                return Command.move(world, Direction.NORTH, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.S, (key) =>
            {
                return Command.move(world, Direction.SOUTH, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.A, (key) =>
            {
                return Command.move(world, Direction.WEST, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.D, (key) =>
            {
                return Command.move(world, Direction.EAST, key);
            }));


            allCommands.Add(new KeyCommand(ConsoleKey.LeftArrow, (key) =>
            {
                return Command.moveSelection(Direction.WEST, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.RightArrow, (key) =>
            {
                return Command.moveSelection(Direction.EAST, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.UpArrow, (key) =>
            {
                return Command.moveSelection(Direction.NORTH, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.DownArrow, (key) =>
            {
                return Command.moveSelection(Direction.SOUTH, key);
            }));


            allCommands.Add(new KeyCommand(ConsoleKey.Subtract, (key) =>
            {
                return Command.moveSelection(Direction.DOWN, key);
            }));
            allCommands.Add(new KeyCommand(0, (key) =>
            {
                return Command.moveSelection(Direction.UP, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.D9, (key) =>
            {
                return Command.moveSelection(Direction.DOWN, key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.D0, (key) =>
            {
                return Command.moveSelection(Direction.UP, key);
            }));


            allCommands.Add(new KeyCommand(ConsoleKey.O, (key) =>
            {
                return Command.selectEntity(-1);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.P, (key) =>
            {
                return Command.selectEntity(1);
            }));


            allCommands.Add(new KeyCommand(ConsoleKey.R, (key) =>
            {
                return Command.rest(key);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.Q, (key) =>
            {
                return Command.mine(world);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.F, (key) =>
            {
                return Command.placeOrUse(world);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.E, (key) =>
            {
                return Command.interact(world);
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.I, (key) =>
            {
                Game.openScreen(new InventoryScreen());
                return true;
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.Escape, (cmd) =>
            {
                Game.printLn(new TranslatableText("command.mode.command.switch"));
                Game.state.mode = Mode.TEXT;
                return true;
            }));



            allCommands.Add(new KeyCommand(ConsoleKey.F2, (key) =>
            {
                return Command.toggleDebug();
            }));
            allCommands.Add(new KeyCommand(ConsoleKey.U, (key) =>
            {
                if (Game.state.debugMode)
                {
                    Game.openScreen(new ChunkDebugScreen());
                    return true;
                }
                return false;
            }));
        }
    }

}