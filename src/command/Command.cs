using System;
using System.Collections.Generic;

namespace cscsg
{

    class Command
    {

        public static List<Command> allCommands;

        public virtual bool match(string input)
        {
            return false;
        }

        public virtual bool matchKey(ConsoleKeyInfo key)
        {
            return false;
        }


        protected static bool move(World world, Direction direction, ConsoleKeyInfo key)
        {
            return move(world, direction,
                key.Modifiers.HasFlag(ConsoleModifiers.Shift) ||
                key.Modifiers.HasFlag(ConsoleModifiers.Control)
            );
        }

        protected static bool move(World world, Direction direction, bool shift = false)
        {
            Player p = Game.player;
            bool shouldTick = false;
            if (shift)
            {
                const int cost = 15;
                if (Game.player.hasEnergy(cost))
                {
                    bool canMove = p.move(world, direction);
                    if (canMove)
                    {
                        p.move(world, direction);
                        Game.player.useEnergy(cost);
                        shouldTick = true;
                    }
                    else
                    {
                        if (p.climb(world, direction))
                        {
                            Game.player.useEnergy(cost);
                            shouldTick = true;
                        }
                    }
                }
                else
                {
                    Game.error(new TranslatableText("command.error.notEnoughEnergy", cost.ToString()));
                }
            }
            else
            {
                int cost = 1;
                if (world.getTile(Game.player.pos).getId() == Tile.WATER)
                {
                    cost++;
                }
                if (Game.player.hasEnergy(cost))
                {
                    if (p.move(world, direction))
                    {
                        Game.player.useEnergy(cost);
                        shouldTick = true;
                    }
                }
                else
                {
                    Game.error(new TranslatableText("command.error.notEnoughEnergy", "1"));
                }
            }
            if (shouldTick)
            {
                Game.setShouldTick();
                if (Game.state.extraInfo)
                {
                    printPlayerPos();
                }
            }
            return true;
        }


        protected static bool moveSelection(Direction direction, ConsoleKeyInfo key)
        {
            return moveSelection(direction,
                key.Modifiers.HasFlag(ConsoleModifiers.Shift) ||
                key.Modifiers.HasFlag(ConsoleModifiers.Control)
            );
        }

        protected static bool moveSelection(Direction direction, bool shift = false)
        {
            if (shift)
            {
                Game.setSelection(Game.getSelection() + Vector3<int>.fromDirection(direction, 3));
            }
            else
            {
                Game.setSelection(Game.getSelection() + Vector3<int>.fromDirection(direction));
            }
            Vector3<int> sel = Game.getAbsoluteSelection();
            if (Game.state.extraInfo)
            {
                printSelPos();
            }
            Game.setSelectedEntityIndex(Game.getSelectedEntityIndex());
            return true;
        }

        protected static bool selectEntity(int delta)
        {
            Game.setSelectedEntityIndex(Game.getSelectedEntityIndex() + delta);
            return true;
        }


        protected static bool rest(ConsoleKeyInfo key)
        {
            return rest(
                key.Modifiers.HasFlag(ConsoleModifiers.Shift) ||
                key.Modifiers.HasFlag(ConsoleModifiers.Control)
            );
        }

        protected static bool rest(bool shift = false)
        {
            if (shift)
            {
                Game.state.restTime = 10;
                Game.state.forceRest = Game.player.hasEnergy(Game.player.getMaxEnergy());
            }
            else
            {
                Game.state.restTime = 2;
            }
            Game.setShouldTick();
            return true;
        }


        protected static bool mine(World world)
        {
            if (Game.player.mineBlock(world, Game.getAbsoluteSelection()))
            {
                Game.setShouldTick();
            }
            return true;
        }

        protected static bool placeOrUse(World world)
        {
            if (!Command.use(world))
            {
                Command.place(world);
            }
            return true;
        }

        protected static bool place(World world)
        {
            if (world.player.placeBlock(world, Game.getAbsoluteSelection()))
            {
                Game.setShouldTick();
            }
            return true;
        }

        // Returns true on success
        protected static bool use(World world)
        {
            if (world.player.useItem(world, Game.getAbsoluteSelection()))
            {
                Game.setShouldTick();
                return true;
            }
            return false;
        }

        protected static bool interact(World world)
        {
            // [TODO] Move to player.interact
            Entity ent = Game.getSelectedEntity();
            if (ent == null)
            {
                return true;
            }
            if (ent.interact(world, Game.player))
            {
                Game.setShouldTick();
            }
            return true;
        }

        protected static bool toggleDebug()
        {
            Game.state.debugMode = !Game.state.debugMode;
            if (Game.state.debugMode)
            {
                Game.printLn(new TranslatableText("debug.debugMode.on"));
                Game.world.cheated = true;
            }
            else
            {
                Game.printLn(new TranslatableText("debug.debugMode.off"));
            }
            return true;
        }



        protected static void printPlayerPos(Vector3<int> p = null)
        {
            if (p == null)
            {
                p = Game.player.pos;
            }
            Game.printLn(new TranslatableText("info.curpos", p.x.ToString(), p.y.ToString(), p.z.ToString()));
        }

        protected static void printSelPos(Vector3<int> p = null)
        {
            if (p == null)
            {
                p = Game.getAbsoluteSelection();
            }
            Game.printLn(new TranslatableText("info.selpos", p.x.ToString(), p.y.ToString(), p.z.ToString()));
        }

        protected static void printPos(Vector3<int> p = null)
        {
            if (p == null)
            {
                p = Game.getAbsoluteSelection();
            }
            Game.printLn(new TranslatableText("info.pos", p.x.ToString(), p.y.ToString(), p.z.ToString()));
        }



        public static void init()
        {
            Command.allCommands = new List<Command>();

            TextCommand.init();
            KeyCommand.init();
        }

    }

}