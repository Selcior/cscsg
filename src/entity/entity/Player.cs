using System.Collections.Generic;
using System;
using System.Text.Json.Nodes;

namespace cscsg
{
    class Player : LivingEntity
    {
        public int mana;
        protected int maxMana;
        public int energy;
        protected int maxEnergy;
        public int deaths = 0;

        public int getMaxMana()
        {
            return this.maxMana;
        }

        public int getMaxEnergy()
        {
            return this.maxEnergy;
        }


        public int reach = 5;

        public Inventory inventory = new Inventory();
        public int selectedItem = 0;

        public string name = null;


        public override string getName()
        {
            if (name != null)
            {
                return name;
            }
            else
            {
                return base.getName();
            }
        }

        public override string getNameTKey()
        {
            return "entity.player.name";
        }


        public override int dealDamage(DamageSource source, int amount, DamageType type, bool critical)
        {
            int r = base.dealDamage(source, amount, type, critical);
            Game.printPersistent(new TranslatableText("battle.damaged", r.ToString()).format(Formatting.RED), 5);
            return r;
        }

        // Returns true if there is enough energy to remove
        public bool useEnergy(int energy)
        {
            if (this.energy >= energy)
            {
                this.energy -= energy;
                return true;
            }
            else
            {
                return false;
            }
        }

        // Returns true if there is enough energy to remove
        public bool hasEnergy(int energy)
        {
            if (this.energy >= energy)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public void addHealth(int health)
        {
            this.health += health;
            if (this.health > this.maxHealth)
            {
                this.health = this.maxHealth;
            }
        }

        public void addMana(int mana)
        {
            this.mana += mana;
            if (this.mana > this.maxMana)
            {
                this.mana = this.maxMana;
            }
        }

        public void addEnergy(int energy)
        {
            this.energy += energy;
            if (this.energy > this.maxEnergy)
            {
                this.energy = this.maxEnergy;
            }
        }

        public void removeHealth(int health)
        {
            this.health -= health;
            if (this.health < 0)
            {
                this.health = 0;
            }
        }

        public void removeMana(int mana)
        {
            this.mana -= mana;
            if (this.mana < 0)
            {
                this.mana = 0;
            }
        }

        public void removeEnergy(int energy)
        {
            this.energy -= energy;
            if (this.energy < 0)
            {
                this.energy = 0;
            }
        }



        public ItemStack getHeldItem()
        {
            if (this.selectedItem >= this.inventory.size || this.selectedItem < 0)
            {
                this.selectedItem = 0;
            }
            return this.inventory.getSlot(selectedItem);
        }

        public void throwItemStack(World world, int slotNumber)
        {
            ItemStack stack = new ItemStack(this.inventory.getSlot(slotNumber));
            this.inventory.removeItem(stack.item, stack.count);
            world.spawnItemStack(stack, this.pos);
        }


        public bool mineBlock(World world, Vector3<int> pos)
        {
            const int cost = 5;
            if (pos.y != this.pos.y)
            {
                if (world.getTile(pos.x, this.pos.y, pos.z).isSolid())
                {
                    if (Math.Abs(pos.x - this.pos.x) + Math.Abs(pos.z - this.pos.z) > 1)
                    {
                        Game.error(new TranslatableText("command.error.mine.cantReach"));
                        return false;
                    }
                }
            }

            Tile tile = world.getTile(pos);
            if (tile.isUnbreakable())
            {
                Game.error(new TranslatableText("command.error.mine.unbreakable", new TranslatableText(tile.getName()).getUnformatted()));
                return false;
            }
            if (this.hasEnergy(cost))
            {
                if (tile.mine(world, pos, this))
                {
                    this.useEnergy(cost);
                    this.giveStacks(world, tile.getLoot(world, this.random));
                    return true;
                }
                else
                {
                    Game.error(new TranslatableText("command.error.mine.error", cost.ToString()));
                    return false;
                }
            }
            else
            {
                Game.error(new TranslatableText("command.error.notEnoughEnergy", cost.ToString()));
                return false;
            }
        }

        public bool placeBlock(World world, Vector3<int> pos)
        {
            const int cost = 5;

            Tile tile = world.getTile(pos);
            if (tile.getId() != Tile.AIR)
            {
                Game.error(new TranslatableText("command.error.place.occupied", new TranslatableText(tile.getName()).getUnformatted()));
                return false;
            }

            ItemStack heldItem = this.getHeldItem();
            if (heldItem == null)
            {
                Game.error(new TranslatableText("command.error.place.noBlockEquipped"));
                return false;
            }
            int heldItemId = heldItem.item;
            if (heldItemId < 256 && heldItemId != 0)
            {
                if (this.useEnergy(cost))
                {
                    if (Tile.fromId((byte)heldItemId).place(world, pos))
                    {
                        this.inventory.removeItem(heldItemId, 1);
                    }
                    return true;
                }
                else
                {
                    Game.error(new TranslatableText("command.error.notEnoughEnergy", cost.ToString()));
                    return false;
                }
            }
            else
            {
                Game.error(new TranslatableText("command.error.place.noBlockEquipped"));
                return false;
            }
        }

        public bool useItem(World world, Vector3<int> pos)
        {
            ItemStack stack = getHeldItem();
            if (stack == null)
            {
                return false;
            }
            Item item = Item.fromId(stack.item);
            bool result = item.use(world, pos, this, stack);
            this.inventory.cleanup();
            return result;
        }


        public void giveStacks(World world, List<ItemStack> stacks)
        {
            foreach (ItemStack stack in stacks)
            {
                this.give(world, stack);
            }
        }

        public void give(World world, ItemStack stack)
        {
            const int pickupTextTicks = 5;

            if (stack.item != Item.NOTHING.getId())
            {
                int amount = this.inventory.addItem(stack.item, stack.count);
                Game.printPersistent(
                    new TranslatableText("info.item.add", new TranslatableText(Item.fromId(stack.item).getName()).getUnformatted(), (stack.count - amount).ToString()),
                    pickupTextTicks
                );

                if (amount > 0)
                {
                    world.spawnItemStack(new ItemStack(stack.item, amount), this.pos);
                    Game.printPersistent(
                        new TranslatableText("info.item.dropped", new TranslatableText(Item.fromId(stack.item).getName()).getUnformatted(), amount.ToString()),
                        pickupTextTicks
                    );
                }
            }
        }


        // Returns true if successfully climbed
        public bool climb(World world, Direction direction)
        {
            Vector3<int> tileInFrontPos = this.pos + Vector3<int>.fromDirection(direction);
            Vector3<int> up = new Vector3<int>(0, 1, 0);

            Tile tileInFront = world.getTile(tileInFrontPos);
            Tile tileAboveInFront = world.getTile(tileInFrontPos + up);
            Tile tileAbove = world.getTile(this.pos + up);

            if (tileInFront.isSolid() && tileInFront.canBeStoodOn() && !tileAbove.isSolid() && !tileAboveInFront.isSolid())
            {
                this.pos = tileInFrontPos + up;
                return true;
            }
            return false;
        }

        public void die(World world)
        {
            Game.openScreen(new GameOverScreen());
            while (this.inventory.getSlot(0) != null)
            {
                this.throwItemStack(world, 0);
            }
            this.pos = new Vector3<int>(0, World.SURFACE_LEVEL + 2, 0);
            this.maxHealth = 100;
            this.health = maxHealth;
            this.maxMana = 100;
            this.mana = 0;
            this.maxEnergy = 2000;
            this.energy = 1000;
            this.deaths++;
            world.autoUnload();
            world.autoLoad();
            this.fallInstant(world);
            Game.clearText();
        }


        public override void tick(World world)
        {
            base.tick(world);
        }


        public override TileChar getTileChar()
        {
            return tileChar;
        }

        private static readonly TileChar tileChar = new TileChar(new Char("🧍", "ඞ", "@"), Formatting.RED); // ඞ ☥


        public Player() : base(Entity.PLAYER)
        {
            this.pos = new Vector3<int>(0, World.SURFACE_LEVEL + 2, 0);
            this.maxHealth = 100;
            this.health = maxHealth;
            this.maxMana = 100;
            this.mana = 0;
            this.maxEnergy = 2000;
            this.energy = 1000;
        }





        public static new Player fromJson(JsonObject j)
        {
            Player result = new Player();
            result.loadJson(j);
            return result;
        }

        public override bool loadJson(JsonObject j)
        {
            base.loadJson(j);
            JsonHelper.load(j, "mana", ref this.mana);
            JsonHelper.load(j, "energy", ref this.energy);
            this.inventory = Inventory.fromJson(JsonHelper.loadObject(j, "inventory"));
            JsonHelper.load(j, "name", ref this.name);
            JsonHelper.load(j, "deaths", ref this.deaths);
            return true;
        }

        public override JsonObject toJson()
        {
            JsonObject j = base.toJson();
            j.Add("mana", JsonValue.Create(this.mana));
            j.Add("energy", JsonValue.Create(this.energy));
            j.Add("inventory", this.inventory.toJson());
            j.Add("name", JsonValue.Create(this.name));
            j.Add("deaths", JsonValue.Create(this.deaths));
            return j;
        }
    }
}