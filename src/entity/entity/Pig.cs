using System.Text.Json.Nodes;

namespace cscsg
{
    class Pig : Animal
    {
        public override string getNameTKey()
        {
            return "entity.pig.name";
        }

        public override TileChar getTileChar()
        {
            return new TileChar(new Char("🐖", "@", "@"), Formatting.MAGENTA);
        }

        public Pig() : base(Entity.PIG, new DumbAi())
        {
            this.maxHealth = 10;
            this.health = maxHealth;
        }

        

        public static new Entity fromJson(JsonObject j)
        {
            Pig result = new Pig();
            result.loadJson(j);
            return result;
        }
    }
}
