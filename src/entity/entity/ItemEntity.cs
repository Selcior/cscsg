using System.Text.Json.Nodes;

namespace cscsg
{
    class ItemEntity : Entity
    {
        public static TileChar tileChar = new TileChar('$', Formatting.WHITE);
        public ItemStack stack;

        public override string getName()
        {
            Item item = Item.fromId(stack.item);
            if (item != null)
            {
                return new TranslatableText("entity.itemStack.name.item",
                    new TranslatableText(item.getName()).getUnformatted(),
                    stack.count.ToString()
                ).getUnformatted();
            }
            else
            {
                return base.getName();
            }
        }

        public override string getNameTKey()
        {
            return "entity.itemStack.name";
        }

        public override TileChar getTileChar()
        {
            return tileChar;
        }

        public override bool canDespawn()
        {
            return false;
        }

        public override bool interact(World world, Player player)
        {
            player.give(world, this.stack);
            this.despawn(world);
            return true;
        }

        public ItemEntity(ItemStack stack = null) : base(Entity.ITEM_STACK)
        {
            this.stack = stack;
        }





        public static new ItemEntity fromJson(JsonObject j)
        {
            ItemEntity result = new ItemEntity();
            result.loadJson(j);
            return result;
        }

        public override bool loadJson(JsonObject j)
        {
            base.loadJson(j);
            this.stack = ItemStack.fromJson(JsonHelper.loadObject(j, "stack"));
            return true;
        }

        public override JsonObject toJson()
        {
            JsonObject j = base.toJson();
            j.Add("stack", this.stack.toJson());
            return j;
        }
    }
}