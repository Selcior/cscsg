using System;
using System.Text.Json.Nodes;

namespace cscsg
{
    abstract class LivingEntity : Entity
    {
        public int health = 1;
        protected int maxHealth = 1;

        public virtual int getMaxHealth()
        {
            return this.maxHealth;
        }

        public int getDefense(DamageType type)
        {
            return 0;
        }

        // Returns the amount of health subtracted from the entity health
        public virtual int dealDamage(DamageSource source, int amount, DamageType type, bool critical)
        {
            float defense = getDefense(type);

            const int a = 40;
            const float b = 1.7f;
            const float c = 10;

            // float damageReduction = defense / 50f;
            float damageReduction = b * (float)Math.Log((defense / a) + 1);
            if (damageReduction > 0.95f)
            {
                damageReduction = 0.95f;
            }

            float damageMultiplier = 1f - damageReduction;

            float bonusDamageReduction = c / (float)amount;

            int actualDamage = amount;
            actualDamage = (int)(damageMultiplier * actualDamage);
            actualDamage -= (int)(bonusDamageReduction * defense);

            if (actualDamage < 1)
            {
                actualDamage = 1;
            }

            if (critical) {
                actualDamage = (int)((double)actualDamage * ((this.random.NextDouble() / 2.0) + 1.5));
            }

            health -= actualDamage;
            return amount;
        }

        public LivingEntity(byte typeId) : base(typeId)
        {
        }



        public override bool loadJson(JsonObject j)
        {
            base.loadJson(j);
            JsonHelper.load(j, "health", ref this.health);
            return true;
        }

        public override JsonObject toJson()
        {
            JsonObject j = base.toJson();
            j.Add("health", JsonValue.Create(this.health));
            return j;
        }
    }
}