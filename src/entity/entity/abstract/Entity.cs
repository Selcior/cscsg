using System;
using System.Text.Json.Nodes;

namespace cscsg
{
    class Entity : JsonSerializable
    {
        private byte typeId;
        private Guid uuid;

        public int age = 0;
        public Vector3<int> pos = new Vector3<int>();
        private bool despawned = false;
        public Random random;


        public byte getTypeId()
        {
            return this.typeId;
        }

        public Guid getUuid()
        {
            return this.uuid;
        }

        public virtual bool isDespawned()
        {
            return despawned;
        }


        public virtual string getName()
        {
            return new TranslatableText(getNameTKey()).getUnformatted();
        }

        public virtual string getNameTKey()
        {
            return "entity.unknown.name";
        }

        public virtual MobcapType getMobcapType()
        {
            return MobcapType.NONE;
        }

        public virtual bool canDespawn()
        {
            return true;
        }


        protected virtual void fall(World world)
        {
            Tile tile = world.getTile(this.pos.x, this.pos.y - 1, this.pos.z);
            if (!tile.canBeStoodOn())
            {
                this.pos.y--;
            }
        }

        protected virtual void fallInstant(World world)
        {
            Tile tile = world.getTile(this.pos.x, this.pos.y - 1, this.pos.z);
            while (!tile.canBeStoodOn())
            {
                this.pos.y--;
                tile = world.getTile(this.pos.x, this.pos.y - 1, this.pos.z);
            }
        }

        // Returns true if successfully moved
        public virtual bool move(World world, Direction direction)
        {
            Vector3<int> dir = Vector3<int>.fromDirection(direction);
            Tile tile = world.getTile(this.pos + dir);
            if (!tile.isSolid())
            {
                this.pos = this.pos + dir;
                return true;
            }
            return false;
        }

        public virtual void tick(World world)
        {
            this.age++;

            this.fall(world);

            Tile tile = Game.world.getTile(this.pos);
            tile.entityCollision(world, this.pos, this);
        }

        public virtual void spawn(World world)
        {
            this.fallInstant(world);
            this.random = new Random(world.getEntitySeed());
        }

        public virtual void despawn(World world) {
            this.despawned = true;
        }

        // Returns true if the interaction should tick the world
        public virtual bool interact(World world, Player player)
        {
            return false;
        }


        public virtual TileChar getTileChar()
        {
            return new TileChar('!', Formatting.MAGENTA);
        }


        protected Entity(byte typeId)
        {
            this.typeId = typeId;
            this.uuid = Guid.NewGuid();
        }


        public const byte UNKNOWN = 0;
        public const byte PLAYER = 1;
        public const byte PIG = 2;
        public const byte ITEM_STACK = 3;
        public const byte ZOMBIE = 4;





        // Returns null for invalid entities
        public static Entity fromJson(JsonObject j)
        {
            byte typeId = 0;
            JsonHelper.load(j, "typeId", ref typeId);

            Entity entity = null;
            switch (typeId)
            {
                case Entity.UNKNOWN:
                case Entity.PLAYER:
                    break;

                case Entity.PIG:
                    entity = Pig.fromJson(j);
                    break;

                case Entity.ITEM_STACK:
                    entity = ItemEntity.fromJson(j);
                    break;

                case Entity.ZOMBIE:
                    entity = Zombie.fromJson(j);
                    break;
            }
            return entity;
        }

        public virtual bool loadJson(JsonObject j)
        {
            JsonHelper.load(j, "typeId", ref this.typeId);
            JsonHelper.load(j, "uuid", ref this.uuid);
            this.pos = Vector3<int>.fromJson(JsonHelper.loadObject(j, "pos"));
            if (this.pos == null) this.pos = new Vector3<int>(0, World.SURFACE_LEVEL + 1, 0);
            JsonHelper.load(j, "age", ref this.age);
            return true;
        }

        public virtual JsonObject toJson()
        {
            JsonObject j = new JsonObject();
            j.Add("typeId", JsonValue.Create(this.typeId));
            j.Add("uuid", JsonValue.Create(this.uuid));
            j.Add("pos", this.pos.toJson());
            j.Add("age", JsonValue.Create(this.age));
            return j;
        }
    }
}