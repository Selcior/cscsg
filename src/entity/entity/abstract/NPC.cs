namespace cscsg
{
    class NPC : LivingEntity
    {
        protected Ai ai;

        public override void tick(World world)
        {
            base.tick(world);
            ai.tick(this, world);
        }

        public NPC(byte typeId, Ai ai) : base(typeId)
        {
            this.ai = ai;
        }
    }
}