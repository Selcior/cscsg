namespace cscsg
{
    class Animal : NPC
    {
        public override MobcapType getMobcapType()
        {
            return MobcapType.ANIMAL;
        }

        public override bool canDespawn()
        {
            return false;
        }

        public Animal(byte typeId, Ai ai) : base(typeId, ai)
        {
        }
    }
}