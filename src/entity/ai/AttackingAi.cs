using System;
using System.Collections.Generic;

namespace cscsg
{
    class AttackingAi : DumbAi
    {
        const int MAX_ITERATIONS = 1000;

        public List<Direction> path = new List<Direction>();
        public int lastPathCheck;
        public int range;


        struct PathCell
        {
            public int x;
            public int z;
            public Direction from;

            public PathCell(int x = 0, int z = 0, Direction from = Direction.INVALID)
            {
                this.x = x;
                this.z = z;
                this.from = from;
            }
        }



        public int getPathCheckInterval(Entity entity)
        {
            return entity.random.Next(10, 30);
        }

        public bool isSpaceSafe(World world, Entity entity, int x, int y, int z)
        {
            if (world.getTile(x, y, z).isSolid())
            {
                return false;
            }
            if (!world.getTile(x, y - 1, z).canBeStoodOn())
            {
                return false;
            }

            if (x == entity.pos.x && z == entity.pos.z)
            {
                return true;
            }
            else
            {
                List<Entity> entities = world.getEntitiesAt(x, y, z);
                foreach (Entity e in entities)
                {
                    if (e.getTypeId() == Entity.ZOMBIE)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void findPath(Entity entity, World world, Vector3<int> target)
        {
            // Game.printLn("findPath()");

            this.path = new List<Direction>();
            this.lastPathCheck = getPathCheckInterval(entity);

            Vector3<int> start = new Vector3<int>(entity.pos);

            // Generate path

            // Dict of already visited tiles
            // Key is coordinate of cell, Value is from which direction the algorithm entered the cell
            Dictionary<long, Direction> checkedChells = new Dictionary<long, Direction>();

            // Store cells that have to be checked
            Queue<PathCell> cellQueue = new Queue<PathCell>();
            cellQueue.Enqueue(new PathCell(start.x, start.z));

            PathCell cell;
            for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                if (cellQueue.Count == 0)
                {
                    break;
                }
                cell = cellQueue.Dequeue();

                // This should never be true and yet sometimes it is
                if (checkedChells.ContainsKey(World.toCoordsId(cell.x, cell.z)))
                {
                    continue;
                }

                if (cell.x == target.x && cell.z == target.z)
                {
                    goto done;
                }

                if (!isSpaceSafe(world, entity, cell.x, entity.pos.y, cell.z))
                {
                    checkedChells.Add(World.toCoordsId(cell.x, cell.z), Direction.INVALID);
                }
                else
                {
                    checkedChells.Add(World.toCoordsId(cell.x, cell.z), cell.from);

                    PathCell cellNorth = new PathCell(cell.x, cell.z - 1, Direction.SOUTH);
                    if (!cellQueue.Contains(cellNorth) && !checkedChells.ContainsKey(World.toCoordsId(cellNorth.x, cellNorth.z)))
                    {
                        cellQueue.Enqueue(cellNorth);
                    }

                    PathCell cellSouth = new PathCell(cell.x, cell.z + 1, Direction.NORTH);
                    if (!cellQueue.Contains(cellSouth) && !checkedChells.ContainsKey(World.toCoordsId(cellSouth.x, cellSouth.z)))
                    {
                        cellQueue.Enqueue(cellSouth);
                    }

                    PathCell cellWest = new PathCell(cell.x - 1, cell.z, Direction.EAST);
                    if (!cellQueue.Contains(cellWest) && !checkedChells.ContainsKey(World.toCoordsId(cellWest.x, cellWest.z)))
                    {
                        cellQueue.Enqueue(cellWest);
                    }

                    PathCell cellEast = new PathCell(cell.x + 1, cell.z, Direction.WEST);
                    if (!cellQueue.Contains(cellEast) && !checkedChells.ContainsKey(World.toCoordsId(cellEast.x, cellEast.z)))
                    {
                        cellQueue.Enqueue(cellEast);
                    }
                }
            }

            // No success :( too many iterations or the queue is empty
            // Game.error("Path not found");
            return;

        done:
            // Found successfully, generate the directions
            while ((cell.x != start.x || cell.z != start.z) && cell.from != Direction.INVALID)
            {
                Direction back = cell.from.invert();
                this.path.Add(back);

                Vector3<int> previousPosition = new Vector3<int>(cell.x, 0, cell.z) + Vector3<int>.fromDirection(cell.from);
                cell = new PathCell(previousPosition.x, previousPosition.z, checkedChells[World.toCoordsId(previousPosition.x, previousPosition.z)]);
            }
            // Game.printLn("Path generated");

        }

        public void usePath(Entity entity, World world)
        {
            Direction dir = path[0];
            entity.move(world, path[0]);
            path.RemoveAt(0);
        }

        public override void tick(Entity entity, World world)
        {
            if (this.lastPathCheck == -1)
            {
                this.lastPathCheck = getPathCheckInterval(entity);
            }

            // Use DumbAi if there are many zombies on one tile
            List<Entity> entities = world.getEntitiesAt(entity.pos.x, entity.pos.y, entity.pos.z);
            foreach (Entity e in entities)
            {
                if (e.getTypeId() == Entity.ZOMBIE && e.getUuid() != entity.getUuid())
                {
                    base.tick(entity, world);
                    return;
                }
            }


            // Check if the player is nearby, find a new path if they are
            int deltaX = entity.pos.x - world.player.pos.x;
            int deltaZ = entity.pos.z - world.player.pos.z;
            double dist = Math.Sqrt((deltaX * deltaX) + (deltaZ * deltaZ));


            // Attack
            if (dist <= 1)
            {
                if (world.player.pos.y == entity.pos.y)
                {
                    world.player.dealDamage(new EntityDamageSource(entity), 10, DamageType.GENERIC, false);
                    return;
                }
            }

            // Use existing path if one exists
            lastPathCheck--;
            if (lastPathCheck > 0)
            {
                if (path.Count > 0)
                {
                    usePath(entity, world);
                    return;
                }
                else
                {
                    base.tick(entity, world);
                    return;
                }
            }

            // Use DumbAi if the player is on a different y
            if (world.player.pos.y != entity.pos.y)
            {
                base.tick(entity, world);
                return;
            }

            // Use DumbAi if the player is out of range
            if (dist > range)
            {
                base.tick(entity, world);
                return;
            }

            // Find a new path
            findPath(entity, world, world.player.pos);
        }

        public AttackingAi(int range = 10)
        {
            this.lastPathCheck = -1;
            this.range = range;
        }
    }
}