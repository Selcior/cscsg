namespace cscsg
{
    class DumbAi : Ai
    {
        public override void tick(Entity entity, World world)
        {
            Direction[] directions = new Direction[] { Direction.NORTH, Direction.SOUTH, Direction.EAST, Direction.WEST };
            int index = entity.random.Next(0, directions.Length + 2);
            if (index < directions.Length)
            {
                Direction dir = directions[index];
                entity.move(world, dir);
            }
        }
    }
}