namespace cscsg
{
    class EntityDamageSource : DamageSource
    {
        public Entity entity = null;
        public Item weapon = Item.NOTHING;

        public EntityDamageSource(Entity entity)
        {
            this.entity = entity;
        }
    }
}