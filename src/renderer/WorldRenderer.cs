using System.Text;
using System;
using System.Collections.Generic;

namespace cscsg
{
    class WorldRenderer
    {
        public List<string> bottomText = new List<string>();
        public List<PersistentText> persistentText = new List<PersistentText>();
        public Camera camera = new Camera();

        public void printLn(string text)
        {
            bottomText.Add(text);
        }

        public void printPersistent(PersistentText text)
        {
            persistentText.Add(text);
        }

        public void clearText()
        {
            bottomText.Clear();
            persistentText.Clear();
        }

        public void drawWorld(World world)
        {
            this.drawWorld(world, Game.settings.getScreenHeight());
        }

        public void drawWorld(World world, int screenHeight, bool clear = false)
        {
            Settings s = Game.settings;

            StringBuilder sb = new StringBuilder();
            sb.Append(Formatting.RESET_STR);
            if (screenHeight <= 1)
            {
                screenHeight = 1;
            }
            string[] screen = new string[screenHeight];

            this.camera.position = new Vector3<int>(Game.player.pos);

            // Camera view rectangle
            Rect<int> r = this.camera.getRect(s.getScreenWidth() / (s.textDoubleWidth ? 2 : 1), screenHeight, s.getScreenHeight());

            Vector3<int> selection = Game.getAbsoluteSelection();

            for (int y = r.y; y < r.y + r.h; y++)
            {
                bool selectionInLine = y == selection.z;

                List<Entity> entitiesInLine = world.getEntities().FindAll(delegate (Entity e)
                {
                    /*
                    y=5                        o <= invisible entity
                    y=4 camera =>       c       e <= visible entity
                    y=3 floor level =>  -------  e
                    y=2                           o
                    */
                    return e.pos.z == y && e.pos.y <= camera.position.y && e.pos.y >= camera.position.y - 1;
                });

                Formatting lastFormatting = null;

                for (int x = r.x; x < r.x + r.w; x++)
                {
                    // Get shown tile and entity
                    Entity entity = null;

                    TileChar tileChar;
                    Tile tile = world.getTile(x, camera.position.y, y);
                    if (tile.getId() != Tile.AIR)
                    {
                        // Tile - wall
                        tileChar = tile.getWallChar(world, x, camera.position.y, y);

                        if (!tile.hasPriority())
                        {
                            if (entitiesInLine.Count > 0)
                            {
                                // Entity - on floor; in wall
                                entity = entitiesInLine.Find(delegate (Entity e)
                                {
                                    return e.pos.x == x && e.pos.y == camera.position.y;
                                });
                            }
                        }
                    }
                    else
                    {
                        // Tile - floor
                        tile = world.getTile(x, camera.position.y - 1, y);
                        tileChar = tile.getFloorChar(world, x, camera.position.y - 1, y);
                        if (entitiesInLine.Count > 0)
                        {
                            if (tile.getId() != Tile.AIR)
                            {
                                // Entity - on floor
                                entity = entitiesInLine.Find(delegate (Entity e)
                                {
                                    return e.pos.x == x && e.pos.y == camera.position.y;
                                });
                            }
                            else
                            {
                                // Entity - below floor
                                entity = entitiesInLine.Find(delegate (Entity e)
                                {
                                    return e.pos.x == x && e.pos.y == camera.position.y - 1;
                                });
                            }
                        }
                    }



                    // Draw tile and entity on screen
                    bool isInSelection = selectionInLine && selection.x == x;

                    if (isInSelection && Game.settings.textColor == 0)
                    {
                        if (Game.settings.textDoubleWidth)
                        {
                            sb.Append("[]");
                        }
                        else
                        {
                            sb.Append("#");
                        }
                    }
                    else
                    {
                        if (entity == null)
                        {
                            if (!tileChar.format.isEqualTo(lastFormatting))
                            {
                                tileChar.format.getDifference(lastFormatting, sb);
                                lastFormatting = new Formatting(tileChar.format);
                            }
                            if (isInSelection)
                            {
                                sb.Append(Formatting.REVERSED);
                                lastFormatting = null;
                            }

                            sb.Append(tileChar.c);
                            if (s.textDoubleWidth)
                            {
                                sb.Append(tileChar.c);
                            }
                        }
                        else
                        {
                            lastFormatting = null;
                            TileChar entityChar = entity.getTileChar();

                            if (entityChar.c.doubleChar != null && s.textDoubleWidth && s.textEmoji)
                            {
                                sb.Append(tileChar.format.background.getBackgroundCode())
                                    .Append(entityChar.format).Append(entityChar.c.doubleChar)
                                    .Append(Formatting.RESET_STR);
                            }
                            else
                            {
                                sb.Append(tileChar.format.background.getBackgroundCode())
                                    .Append(entityChar.format).Append(entityChar.c)
                                    .Append(Formatting.RESET_STR);
                                if (s.textDoubleWidth)
                                {
                                    lastFormatting = new Formatting(tileChar.format);
                                    sb.Append(tileChar.format).Append(tileChar.c);
                                }
                            }
                        }

                        if (isInSelection)
                        {
                            sb.Append(Formatting.RESET_STR);
                            lastFormatting = null;
                        }
                    }
                }
                sb.Append(Formatting.RESET_STR);
                sb.Append('\n');
            }
            if (clear)
            {
                Console.Clear();
            }
            Console.Write(sb.ToString());
        }

        public void drawBottomText()
        {
            StringBuilder sb = new StringBuilder();
            for (int y = 0; y < bottomText.Count; y++)
            {
                sb.Append(bottomText[y])
                    .Append(Formatting.RESET_STR)
                    .Append('\n');
            }

            Console.Write(Formatting.RESET_STR);
            Console.Write(sb.ToString());
            bottomText.Clear();
        }

        public void drawPrompt(World world)
        {
            // Unicode symbols: █ 

            Settings s = Game.settings;
            Player p = Game.player;

            Color bgColor = Color.BLACK;
            if (s.textColor >= ColorLevel.COLOR_256)
            {
                bgColor = Color.fromGrayscale(0.20f);
            }
            Formatting bgFormat = new Formatting().setBackground(bgColor);

            Color bgColor2 = Color.BLACK;
            if (s.textColor >= ColorLevel.COLOR_256)
            {
                bgColor2 = Color.fromGrayscale(0.40f);
            }
            Formatting bgFormat2 = new Formatting(Color.WHITE).setBackground(bgColor2);

            Color bgColor3 = Color.GREEN;
            Formatting bgFormat3 = new Formatting(Color.WHITE).setBackground(bgColor3);

            Tile selTile = world.getTile(Game.getAbsoluteSelection());
            int selectionY = Game.getSelection().y;
            ItemStack heldItem = Game.player.getHeldItem();
            Entity targetedEntity = Game.getSelectedEntity();
            int targetedEntityIndex = Game.getSelectedEntityIndex();
            int targetedEntityCount = Game.getSelectedEntities().Count;


            Color lastBgColor = bgColor;

            char filledTriangle;
            char triangleSeparator;

            if (s.textUnicode)
            {
                filledTriangle = '';
                triangleSeparator = '';
            }
            else
            {
                filledTriangle = '>';
                triangleSeparator = '>';
            }

            Text text;
            if (Game.state.extraInfo)
            {
                text = new LiteralText($" {p.name} ").format(Formatting.REVERSED)
                    .append($"{filledTriangle} ", bgFormat)
                    .append(new TranslatableText("prompt.health.full", p.health.ToString(), p.getMaxHealth().ToString())
                        .format(new Formatting(Color.RED).setBackground(bgColor)))
                    .append("  ", bgFormat)
                    .append(new TranslatableText("prompt.mana.full", p.mana.ToString(), p.getMaxMana().ToString())
                        .format(new Formatting(Color.BLUE).setBackground(bgColor)))
                    .append("  ", bgFormat)
                    .append(new TranslatableText("prompt.energy.full", p.energy.ToString(), p.getMaxEnergy().ToString())
                        .format(new Formatting(Color.YELLOW).setBackground(bgColor)))
                    .append(" ", bgFormat);
            }
            else
            {
                text = new LiteralText($" {p.name} ").format(Formatting.REVERSED)
                    .append($"{filledTriangle} ", bgFormat)
                    .append(new TranslatableText("prompt.health", p.health.ToString())
                        .format(new Formatting(Color.RED).setBackground(bgColor)))
                    .append("  ", bgFormat)
                    .append(new TranslatableText("prompt.mana", p.mana.ToString())
                        .format(new Formatting(Color.BLUE).setBackground(bgColor)))
                    .append("  ", bgFormat)
                    .append(new TranslatableText("prompt.energy", p.energy.ToString())
                        .format(new Formatting(Color.YELLOW).setBackground(bgColor)))
                    .append(" ", bgFormat);
            }

            if (selTile.getId() != Tile.AIR || selectionY != 0)
            {
                text.append($"{filledTriangle} ", new Formatting(lastBgColor).setBackground(bgColor2))
                .append(new TranslatableText(selTile.getName()).format(bgFormat2))
                .append(" ", bgFormat2)
                .append(new TranslatableText("prompt.selectionY", selectionY.ToString()).format(bgFormat2))
                .append(" ", bgFormat2);
                lastBgColor = bgColor2;
            }
            if (targetedEntity != null)
            {
                text.append($"{filledTriangle} ", new Formatting(lastBgColor).setBackground(bgColor3))
                .append(new LiteralText(targetedEntity.getName()).format(bgFormat3))
                .append(" ", bgFormat3);
                if (Game.state.debugMode)
                {
                    text.append(new LiteralText(targetedEntity.getUuid().ToString()).format(bgFormat3))
                    .append(" ", bgFormat3);
                }
                if (targetedEntity is LivingEntity)
                {
                    text.append(new TranslatableText("prompt.targetedEntity.health",
                        ((LivingEntity)targetedEntity).health.ToString(),
                        ((LivingEntity)targetedEntity).getMaxHealth().ToString()
                    ).format(bgFormat3))
                    .append(" ", bgFormat3);
                }
                if (targetedEntityCount > 1)
                {
                    text.append(new TranslatableText("prompt.targetedEntity",
                        (targetedEntityIndex + 1).ToString(),
                        targetedEntityCount.ToString()
                    ).format(bgFormat3))
                    .append(" ", bgFormat3);
                }

                text.append($"{filledTriangle} ", new Formatting(bgColor3));
                lastBgColor = bgColor3;
            }
            else
            {
                text.append($"{filledTriangle} ", new Formatting(lastBgColor));
            }

            if (heldItem != null)
            {
                text.append(new TranslatableText("prompt.heldItem",
                    new TranslatableText(Item.fromId(heldItem.item).getName()).get(),
                    heldItem.count.ToString()
                )).append($" {triangleSeparator} ");
            }
            Console.Write(text.get());
            Console.Write(Formatting.RESET_STR);
        }

        public void renderNormal(World world)
        {
            Settings s = Game.settings;
            if (Game.state.extraInfo)
            {
                Game.printLn(new TranslatableText("info.tickNumber", world.getAge().ToString()));
            }

            for (int i = 0; i < this.persistentText.Count; i++)
            {
                PersistentText elem = this.persistentText[i];
                if (elem.tick())
                {
                    this.persistentText.RemoveAt(i);
                    i--;
                    continue;
                }
                this.bottomText.Insert(0, elem.text);
            }

            int worldRenderHeight = s.getScreenHeight() - bottomText.Count - 1;
            drawWorld(world, worldRenderHeight, true);
            drawBottomText();
            drawPrompt(world);
        }
    }
}