namespace cscsg
{
    class Char
    {
        public string doubleChar = null;
        public string normal = null;
        public string noUnicode = null;

        public Char()
        {
        }

        public Char(char normal)
        {
            this.normal = normal.ToString();
        }

        public Char(string normal)
        {
            this.normal = normal;
        }

        public Char(string normal, string noUnicode)
        {
            this.normal = normal;
            this.noUnicode = noUnicode;
        }

        public Char(string doubleChar, string normal, string noUnicode)
        {
            this.doubleChar = doubleChar;
            this.normal = normal;
            this.noUnicode = noUnicode;
        }

        public override string ToString()
        {
            if (this.noUnicode == null)
            {
                return this.normal;
            }
            else
            {
                return Game.settings.textUnicode ? this.normal : this.noUnicode;
            }
        }
    }
}