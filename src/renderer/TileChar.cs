namespace cscsg
{
    class TileChar
    {
        public Char c = new Char(' ');
        public Formatting format = Formatting.RESET;

        public TileChar()
        {

        }

        public TileChar(char c, Formatting format) : this(new Char(c), format) { }

        public TileChar(string c, Formatting format) : this(new Char(c), format) { }

        public TileChar(Char c, Formatting format)
        {
            this.c = c;
            this.format = format;
        }
    }
}