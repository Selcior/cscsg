{
    "hello": "Hello, %0!",

    "intro.name": "Enter player name:",
    "intro.worlds": "Found worlds:",
    "intro.world": "- %0",
    "intro.worldName": "Enter world name:",
    "intro.seed": "Enter world seed:",

    "debug.debugMode.on": "Enabled DEBUG MODE",
    "debug.debugMode.off": "Disabled DEBUG MODE",
    "debug.markedChunks": "Marked old chunks for unloading",
    "debug.unhandledKey": "Unhandled key: %0 | %1",

    "error.errorMessage": "Error: ",
    "error.whileLoadingFile": "While loading file %0:",
    "error.mode.key.unavaliable": "KEY MODE is unavaliable on this computer.",
    "error.mode.invalid": "Invalid mode. Switching to TEXT MODE.",
    "error.settings.load": "Could not load settings.json. Please make sure that settings.json exists and is in the correct format.",
    "error.world.load.wrongVersion": "Could not load the world. The world you were trying to load is incompatible with this version of the game.",
    "error.world.save.chunk": "Error while saving chunk: ",

    "info.generatedChunks": "Generated new chunks (%0)",
    "info.generatedChunks.whileLoading": "Generated new chunks (%0) while loading",
    "info.tickNumber": "Tick #%0",
    "info.autosave": "Autosaved world \"%0\"",
    "info.save": "Saved world \"%0\"",
    "info.item.add": "Picked up %1x %0",
    "info.item.dropped": "Dropped %1x %0",
    "info.curpos": "Current position: [%0, %1, %2]",
    "info.selpos": "Selection position: [%0, %1, %2]",
    "info.pos": "Position: [%0, %1, %2]",
    "info.seed": "World seed: %0",

    "screen.undefined.title": "Screen",
    "screen.pager.hint.key": "[Esc] Quit, [ArrowUp] [ArrowDown] Scroll",
    "screen.pager.hint.text": "`q` Quit, `u` `d` Scroll up/down",
    "screen.help.title": "Help",
    "screen.help.general.title": "General help",
    "screen.help.text.title": "TEXT MODE commands",
    "screen.help.text.debug": "== Debug commands == ",
    "screen.help.key.title": "KEY MODE commands",
    "screen.help.key.debug": "== Debug commands == ",
    "screen.help.hint.key": "[Esc] Quit, [ArrowUp] [ArrowDown] Scroll, [ArrowLeft] [ArrowRight] Previous/Next page",
    "screen.help.hint.text": "`q` Quit, `u` `d` Scroll up/down, `p` `n` Previous/Next page",
    "screen.colors.title": "Color List",
    "screen.colors.color": "Color #%0: ",
    "screen.colors.grayscale": "Grayscale #%0: ",
    "screen.tiles.title": "Tile List",
    "screen.tiles.tile": "Tile #%0: ",
    "screen.items.title": "Item List",
    "screen.items.item": "Item #%0: ",
    "screen.inventory.title": "Inventory",
    "screen.inventory.item": "%0: %1 x%2/%3",
    "screen.inventory.hint.key": "[Esc] Quit, [ArrowUp] [ArrowDown] Scroll, [T] Throw away, [Space]/[Enter] Select",
    "screen.inventory.hint.text": "`q` Quit, `u` `d` Scroll up/down, `t` Throw away, `s` Select",
    "screen.settings.title": "Settings",
    "screen.settings.hint.key": "[Esc] Quit, [ArrowUp] [ArrowDown] Scroll, [Enter] Set, [A] Apply and save, [R] Reload from file",
    "screen.settings.hint.text": "`q` Quit, `u` `d` Scroll up/down, `s` Set, `a` Apply and save, `r` Reload from file",
    "screen.gameOver.header": "Game over!",
    "screen.gameOver.subtitle": "Press [Enter] to respawn",
    "screen.chunkDebug.title": "Chunk menu",
    "screen.chunkDebug.hint": "[Esc] Quit, [ArrowUp] [ArrowDown] [ArrowLeft] [ArrowRight] Scroll",

    "command.error.notEnoughEnergy": "Not enough energy! You need %0 energy to perform this action",
    "command.error.unknownCommand": "Unknown command",
    "command.error.unknownCommand.suggestion": "Unknown command. Did you mean: %0",
    "command.error.mine.unbreakable": "%0 is unbreakable",
    "command.error.mine.cantReach": "This tile is out of reach",
    "command.error.mine.other": "%0 could not be destroyed",
    "command.error.place.noBlockEquipped": "You cannot place this item",
    "command.error.place.occupied": "This place is already occupied by %0",
    "command.error.place.wrongFloor.grass": "This tile has to be placed on grass",
    "command.error.debugDisabled": "You need to enable debug mode to use this command",
    "command.mode.command.switch": "Switching to TEXT MODE",
    "command.mode.key.switch": "Switching to KEY MODE",


    "help.general.modes.title": "== Modes ==",
    "help.general.modes": "There are two input modes: TEXT MODE and KEY MODE. In TEXT MODE, you input whole commands, and in KEY MODE, you press single keys. KEY MODE is more comfortable to use, but it's not avaliable on every system. You can input `p` to switch to KEY MODE, and press [Escape] to go back to TEXT MODE.",
    "help.general.visibility.title": "== Visibility ==",
    "help.general.visibility": "You see the world from a top-down view. You see only two layers of tiles: the tiles that are in front of you, and the tiles that are below you. You won't see tiles above you, and you won't see tiles below the floor level. The same tiles can look different when they are on a different layer. You can see all the tiles using the `tiles` command.",
    "help.general.cursor.title": "== Cursor ==",
    "help.general.cursor": "To select a tile, you have to move your cursor using the arrow keys (KEY MODE) or using `i`, `j, `k`, `l` commands (TEXT MODE). You can see the name of the selected tile in the prompt. You can also select tiles above or below you by using the [9] and [0] or [-] and [=] keys (KEY MODE) or using the `-` and `=` commands (TEXT MODE). If the selection is not level with the player, the prompt will always show the selected tile, as well as the number of layers the selection is below or above the player in brackets ( ). If you are breaking or placing a tile, and you don't see a change in the world, make sure that your selection is on the correct layer.",
    "help.general.prompt.title": "== Prompt ==",
    "help.general.prompt": "The prompt shows some useful information. In order they are: player's name, player's health, mana and energy, selected tile, targeted entity, held item. The selected tile doesn't display if the targeted tile is air and if it's level with the player. If there is more than one entity in the selected tile, the prompt shows the index of the selected entity and the total count of entities in the selection in square brackets [ ].",

    "help.text.w": "`w` - Move north",
    "help.text.s": "`s` - Move south",
    "help.text.a": "`a` - Move west",
    "help.text.d": "`d` - Move east",
    "help.text.W": "`W` - Dash/Climb north",
    "help.text.S": "`S` - Dash/Climb south",
    "help.text.A": "`A` - Dash/Climb west",
    "help.text.D": "`D` - Dash/Climb east",
    "help.text.i": "`i` - Move selection north",
    "help.text.k": "`k` - Move selection south",
    "help.text.j": "`j` - Move selection west",
    "help.text.l": "`l` - Move selection east",
    "help.text.I": "`I` - Move selection north x3",
    "help.text.K": "`K` - Move selection south x3",
    "help.text.J": "`J` - Move selection west x3",
    "help.text.L": "`L` - Move selection east x3",
    "help.text.-": "`-` - Move selection down",
    "help.text.=": "`=` - Move selection up",
    "help.text.[": "`[` - Select previous entity",
    "help.text.]": "`]` - Select next entity",
    "help.text.r": "`r` - Rest x2",
    "help.text.R": "`R` - Rest x10",
    "help.text.q": "`q` - Mine selected tile",
    "help.text.mine": "`mine` - Mine selected tile",
    "help.text.f": "`f` - Place equipped tile/Use equipped item",
    "help.text.place": "`place` - Place equipped tile",
    "help.text.use": "`use` - Use equipped item",
    "help.text.e": "`e` - Interact with entity/tile",
    "help.text.interact": "`interact` - Interact with entity/tile",
    "help.text.inv": "`inv` - Open inventory",
    "help.text.help": "`help` - Open help screen",
    "help.text.p": "`p` - Switch to KEY MODE",
    "help.text.colors": "`colors` - Open color list screen",
    "help.text.tiles": "`tiles` - Open tile list screen",
    "help.text.items": "`items` - Open item list screen",
    "help.text.info": "`info` - Toggle extra info",
    "help.text.pos": "`pos` - Print current position",
    "help.text.rt": "`rt` - Reload translation file",
    "help.text.save": "`save` - Save the world",
    "help.text.saveas": "`saveas` - Save a copy of the world with a different name",
    "help.text.seed": "`seed` - Print world seed",
    "help.text.settings": "`settings` - Open settings screen",
    "help.text.exit": "`exit` - Exit game",
    "help.text.debug": "`debug` - Toggle Debug Mode",
    "help.text.chkdbg": "`chkdbg` - Open chunk menu",
    "help.text.testd": "`testd` - Open test dialogue",
    "help.text.give": "`give` - Give some items",
    "help.text.givelots": "`givelots` - Give lots of items",
    "help.text.take": "`take` - Delete some items",
    "help.text.spawn": "`spawn` - Spawn a pig",

    "help.key.w": "[W] - Move north",
    "help.key.s": "[S] - Move south",
    "help.key.a": "[A] - Move west",
    "help.key.d": "[D] - Move east",
    "help.key.w.shift": "[Shift + W] - Dash/Climb north",
    "help.key.s.shift": "[Shift + S] - Dash/Climb south",
    "help.key.a.shift": "[Shift + A] - Dash/Climb west",
    "help.key.d.shift": "[Shift + D] - Dash/Climb east",
    "help.key.up": "[ArrowUp] - Move selection north",
    "help.key.down": "[ArrowDown] - Move selection south",
    "help.key.left": "[ArrowLeft] - Move selection west",
    "help.key.right": "[ArrowRight] - Move selection east",
    "help.key.up.shift": "[Shift + ArrowUp] - Move selection north x3",
    "help.key.down.shift": "[Shift + ArrowDown] - Move selection south x3",
    "help.key.left.shift": "[Shift + ArrowLeft] - Move selection west x3",
    "help.key.right.shift": "[Shift + ArrowRight] - Move selection east x3",
    "help.key.subtract": "[-] - Move selection down",
    "help.key.equals": "[=] - Move selection up",
    "help.key.9": "[9] - Move selection down",
    "help.key.0": "[0] - Move selection up",
    "help.key.o": "[O] - Select previous entity",
    "help.key.p": "[P] - Select next entity",
    "help.key.r": "[R] - Rest x2",
    "help.key.r.shift": "[Shift + R] - Rest x10",
    "help.key.q": "[Q] - Mine selected tile",
    "help.key.f": "[F] - Place equipped tile/Use equipped item",
    "help.key.e": "[E] - Interact with entity/tile",
    "help.key.i": "[I] - Open inventory",
    "help.key.h": "[H] - Open help screen",
    "help.key.escape": "[Escape] - Switch to TEXT MODE",
    "help.key.f2": "[F2] - Toggle Debug Mode",
    "help.key.u": "[U] - Open chunk menu",

    "settings.true": "True",
    "settings.false": "False",
    "settings.null": "Null",
    "settings.yes": "Yes",
    "settings.no": "No",
    "settings.auto": "Auto",
    "setting.screenWidth": "Screen Width: %0",
    "setting.screenHeight": "Screen Height: %0",
    "setting.lang": "Language: %0",
    "setting.lang.found": "Found languages:",
    "setting.lang.file": "- %0",
    "setting.lang.input": "Input language name:",
    "setting.textSpeed": "Dialogue Text Speed: %0",
    "setting.textColor": "Color Support: %0",
    "setting.textUnicode": "Allow Unicode: %0",
    "setting.textDoubleWidth": "Double Char Tiles: %0",
    "setting.textEmoji": "Allow Emoji: %0",

    "prompt.health": "%0 ♥",
    "prompt.mana": "%0 ★",
    "prompt.energy": "%0 ⚡",
    "prompt.health.nounicode": "%0 HP",
    "prompt.mana.nounicode": "%0 MA",
    "prompt.energy.nounicode": "%0 EN",
    "prompt.health.full": "%0/%1 ♥",
    "prompt.mana.full": "%0/%1 ★",
    "prompt.energy.full": "%0/%1 ⚡",
    "prompt.health.full.nounicode": "%0/%1 HP",
    "prompt.mana.full.nounicode": "%0/%1 MA",
    "prompt.energy.full.nounicode": "%0/%1 EN",
    "prompt.selectionY": "(%0)",
    "prompt.heldItem": "%0 x%1",
    "prompt.targetedEntity": "[%0/%1]",
    "prompt.targetedEntity.health": "(%0/%1)",

    "tile.air": "Air",
    "tile.grass": "Grass",
    "tile.dirt": "Dirt",
    "tile.stone": "Stone",
    "tile.bedrock": "Bedrock",
    "tile.sapling": "Sapling",
    "tile.tree": "Tree",
    "tile.tallGrass": "Tall Grass",
    "tile.yellowFlower": "Dandelion",
    "tile.redFlower": "Rose",
    "tile.water": "Water",
    "tile.shallowWater": "Shallow Water",
    "tile.sand": "Sand",
    "tile.coalOre": "Coal Ore",
    "tile.goldOre": "Gold Ore",
    "tile.wood": "Wood",
    "tile.forestGrass": "Forest Grass",
    "tile.redMushroom": "Red Mushroom",
    "tile.blueMushroom": "Blue Mushroom",
    "tile.yellowMushroom": "Yellow Mushroom",
    "tile.purpleMushroom": "Purple Mushroom",
    "tile.emeraldOre": "Emerald Ore",
    "tile.rubyOre": "Ruby Ore",
    "tile.diamondOre": "Diamond Ore",
    "tile.cobweb": "Cobweb",
    "tile.id.240": "Gradient Tile 1",
    "tile.id.241": "Gradient Tile 2",
    "tile.id.242": "Gradient Tile 3",
    "tile.id.243": "Gradient Tile 4",
    "tile.id.244": "Gradient Tile 5",
    "tile.id.245": "Gradient Tile 6",
    "tile.id.246": "Gradient Tile 7",
    "tile.id.247": "Gradient Tile 8",
    "tile.id.255": "Invalid Tile",

    "item.stick": "Stick",
    "item.seeds": "Seeds",

    "entity.player.name": "Player",
    "entity.pig.name": "Pig",
    "entity.itemStack.name": "Item Stack",
    "entity.itemStack.name.item": "Item Stack: %0 x%1",
    "entity.zombie.name": "Zombie",

    "battle.damaged": "You got damaged! -%0 HP",

    "dialogue.knight.name": "𝕶𝖓𝖎𝖌𝖍𝖙 𝖔𝖋 𝖙𝖍𝖊 𝕯𝖆𝖗𝖐",
    "dialogue.test": "%f%c{reversed} 𝔎𝔫𝔦𝔤𝔥𝔱 𝔬𝔣 𝔱𝔥𝔢 𝔇𝔞𝔯𝔨 %c{reset}\n%c{reversed}%c{bold} 𝔎𝔫𝔦𝔤𝔥𝔱 𝔬𝔣 𝔱𝔥𝔢 𝔇𝔞𝔯𝔨 %c{reset}\n%c{reversed} 𝕶𝖓𝖎𝖌𝖍𝖙 𝖔𝖋 𝖙𝖍𝖊 𝕯𝖆𝖗𝖐 %c{reset}\n%c{reversed}%c{bold} 𝕶𝖓𝖎𝖌𝖍𝖙 𝖔𝖋 𝖙𝖍𝖊 𝕯𝖆𝖗𝖐 %c{reset}\n%w%wThis is a test of Unicode's cool characters",
    "dialogue.option.continue": " CONTINUE >> ",
    "dialogue.intro": "%c{red}You wake up in an unfamiliar place.%w.%w.%w%w"
}
